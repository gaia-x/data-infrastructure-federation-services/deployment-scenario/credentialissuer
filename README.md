## Table of contents

- [Introduction](#introduction)
  - [Presentation of the ICP (Issuance Credential Protocol) ](#presentation-of-the-icp-issuance-credential-protocol-)
  - [Context](#context)
  - [Objectives of the ICP](#objectives-of-the-icp)
  - [Simplified architecture](#simplified-architecture)
  - [Examples of use](#examples-of-use)
    - [Example 1 - Creation of a VC LegalParticipant](#example-1---creation-of-a-vc-legalparticipant)
    - [Example 2 - Presentation of a credential in SD-JWT format ](#example-2---presentation-of-a-credential-in-sd-jwt-format-)
    - [Example 3 - Compliance Paticipant request ](#example-3---compliance-paticipant-request-)
    - [Example 4 - Revocation of a Compliance Participant VC ](#example-4---revocation-of-a-compliance-participant-vc-)
- [Overview](#overview)
  - [The Role of the Portal component](#the-role-of-the-portal-component)
  - [The Role of the CredentialIssuer component](#the-role-of-the-credentialissuer-component)
  - [The Role of the Gateway Gaia-X component](#the-role-of-the-gateway-gaia-x-component)
  - [The Role of Administrator Portal component](#the-role-of-administrator-portal-component)
- [Architecture](#architecture)
  - [OID4VCI](#oid4vci)
    - [Pre-Authorized workflow](#pre-authorized-workflow)
    - [Authorized workflow](#authorized-workflow)
    - [Batch Credential](#batch-credential)
  - [OID4VP](#oid4vp)
  - [Rest API](#rest-api)
    - [OIDC Route](#oidc-route)
    - [Issuance Route](#issuance-route)
    - [Issuance Route by unique link](#issuance-route-by-unique-link)
    - [Revocation Route](#revocation-route)
      - [Query for Search VC](#query-for-search-vc)
      - [Query for update status of VC](#query-for-update-status-of-vc)
  - [Credential Issuer](#credential-issuer)
  - [Gateway Gaia-X](#gateway-gaia-x)
  - [Types of issuance](#types-of-issuance)
    - [Form Issuance](#form-issuance)
    - [Presentation Issuance](#presentation-issuance)
    - [Keycloak issuance](#keycloak-issuance)
- [Add a plugin for a new type of VC to be issued](#add-a-plugin-for-a-new-type-of-vc-to-be-issued)
- [Technologies and tools used](#technologies-and-tools-used)
- [Installation](#installation)
  - [Setting environment variables](#setting-environment-variables)
    - [CredentialIssuer environment variables](#credentialissuer-environment-variables)
    - [Gateway Gaiax environment variables](#gateway-gaiax-environment-variables)
    - [Portal environment variables](#portal-environment-variables)
  - [For develpment](#for-develpment)
  - [For testing with docker](#for-testing-with-docker)
- [Authors and acknowledgment](#authors-and-acknowledgment)
- [License](#license)

# Introduction

## Presentation of the ICP (Issuance Credential Protocol) <a name="presentation-icp"></a>

The Issuance Credential Protocol (ICP) is a project that aims to bring verifiable credentials (VCs) into the Gaia-X ecosystem (but not only), using standards compliant with the latest OID4VCI, OID4VP and Bitstring Status List v1.0 protocols. This includes the ability to generate and sign verifiable credentials (VCs), as well as manage their lifecycles (suspension, revocation).

To achieve this, ICP implements several components that interact with each other:

- [Portal](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/portal): This is the Web user interface used to make a VC request.
- [CredentialIssuer](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/credentialissuer): This is the ICP backend. It is called by the Portal and handles the construction, signing and lifecycle of VCs requested by users. It also makes requests to the Gaia-X Gateway for VC data.
- [Gaia-X Gateway](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/gatewaygaiax): This component acts as a gateway between the ICP and the various Gaia-X components (Compliance, Labelling, RegistrationNumber, etc.). It makes calls to collect data from the requested VCs, then transmits it to the ICP.
- [Portal Admin](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/adminportal): This is the ICP administrator interface. It lists all the VCs that have been issued, and lets you view and update their status.

## Context

As part of the Gaia-X project, the aim of this tool (optional) is to offer all players wishing to join the Gaia-X ecosystem a simple means of interacting with Gaia-X components and their Wallet.
This tool is deployed by provier and federation, and will enable participating suppliers to obtain, among other things, their LegalParticipant, TermsAndCondition, or ComplianceParticipant VCs.

At the same time, we ensure that the verifiables credentials (VCs) issued are cryptographically secure, and that they can work with all wallets implementing the OID4VCI and OID4VP protocols. This also includes verification of the VC's signature by any actor via their public key.

In addition, the tool supports several Verifiable Credentials formats to meet different user needs:

- **ldp_vc format** (Linked Data Proof VC): A format based on W3C standards using linked proofs to ensure the integrity and authenticity of VCs.  
- **Format jwt_vc_json-ld** (JWT-VC-JSON-LD): A new format for Gaia-X VCs combining JSON Web Tokens (JWT) with JSON-LD (JavaScript Object Notation for Linked Data). This format offers a flexible and extensible structure for VCs while ensuring their security via JWT signatures.  
- **SD-JWT format** (Selective Disclosure JWT): A format allowing selective disclosure, i.e. the ability to reveal only certain parts of the information contained in a VC while keeping the other parts confidential. This is particularly useful for protecting users' privacy by sharing only the information that is strictly necessary.

In the event of a supplier not complying with Gaia-X's terms of use, a Gaia-X administrator can easily carry out searches to suspend the supplier's VCs via the administrator portal.

In addition, the ICP has been designed to be scalable, i.e. it is possible to easily add a new type of VC (see chapter [Add a plugin for a new type of VC to be issued](#add-a-plugin-for-a-new-type-of-vc-to-be-issued)), or to easily modify an already implemented VC by modifying the configuration and not necessarily the code.

## Objectives of the ICP

- Issuing cryptographically secure VCs using the OID4VCI protocol, which works with all wallets implementing this protocol.

- Receive and verify verifiable presentations using the OID4VP protocol.

- Manage the VC lifecycle using the Bitstring Status List v1.0 standard via the Administrator Portal.

- Interact with the various components of the Gaia-X ecosystem and back office via the Gaia-X Gateway.

## Simplified architecture

![Simplified architecture](/documentation/SimplifiedArchitecture.png)

## Examples of use

The ICP is an optional support tool, designed to provide assistance to any participant wishing to join the Gaia-x ecosystem. This tool (ICP) is deployed by actor and by federation. Each tenant can retrieve one or more VCs (verifiable credentials) for a user, and each of these VCs is signed by the tenant in question.

To retrieve a VC, a user must go to the [portal](#the-role-of-the-portal-component). On the portal home page, there's a drop-down menu containing a list of VCs that can be retrieved. The user then chooses a VC type, starts the issue and follows the portal instructions (instructions differ according to VC type).

All VCs issued by the ICP are registered, and can be suspended/revoked at any time from the [AdminPortal](#the-role-of-administrator-portal-component).

Below are three typical scenarios that can be realized by the ICP and its components. Other scenarios can be realized depending on the type of VC chosen by the user.

### Example 1 - Creation of a VC LegalParticipant

- **Context**: Bob is the administrator of Company Dufourstorage, and wants to identify his company as a provider to the GXDCH.

- **Task performed by User**: Bob goes to the portal's Dufourstorage tenant; chooses the VC LegalParticipant from the drop-down menu on the home page; fills in and validates the form that appears on the screen; scans the Qr Code with his wallet to retrieve his VC from LegalParticipant. 

- **Task performed by ICP**: When Bob chooses the VC LegalParticipant, the ICP makes a request to the Gaia-x gateway to retrieve the basic form and transmits it to the portal; Once the form has been validated, the ICP sends the data to the gateway; The gateway validates the data and sends the VC to the ICP; The ICP creates an issuance uri (OIDC4VCI protocol); When bob scans the Qr Code, the ICP signs the VC and transmits it to the BOB wallet (OIDC4VCI protocol).

**This example use the Walt.id wallet**
![LegalParticipantGif](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/credentialissuer/-/raw/main/documentation/LegalParticipant.gif?ref_type=heads)


### Example 2 - Presentation of a credential in SD-JWT format <a name="example-2"></a>

- **Context**: Bob already has a credential in SD-JWT format (EmployeeCredential credential here) in his wallet. And he wants to access a service that asks him to verify that he belongs to his company. To do this, it uses the ICP Verifier

- **Task performed by User**: BOB goes to the PCI "aster-x" portal and selects EmployeeCredential. A Qr code appears on the screen and Bob scans it with his wallet. He then chooses the credential and the fields he wants to send. Some fields are mandatory (in this example first name, family name, and company) and others are optional (in this example birth date). Once Bob has confirmed that he wants to send the data, he receives the result of the verification process.

- **Task performed by ICP**: The ICP makes a request to the Gaia-x gateway to obtain the presentation definition for the EmployeeCredential credential; The ICP builds the issuance link containing the `request_uri` parameter and transmits it to the portal, which transforms it into a QrCode; When the user scans the QrCode, the wallet makes a request to the `request_uri` endpoint; The PICP, which receives the request_uri request, builds and signs the authorisation object in JWT format, which contains the `presentation_definition`, the `client_id_scheme`, the `response_uri`, and other metadata. This object is returned in response to the `request_uri` request; The wallet receives the authorisation object containing the presentation definition, and displays the credentials corresponding to the user; When the user select the fields and clicks on 'send', the wallet builds and sends the `vp_token` to the ICP using the `response_uri` contained in the authorization object ; The ICP receives the `vp_token`, verifies it (signature, expiration date of credentials, revocation); Then sends the result to the portal (which contains whether the `vp_token` is valid or not, as well as the `vp_token` data.

**This example use the Talao wallet**

![PresentationSD-JWT](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/credentialissuer/-/raw/main/documentation/PresentationSD-JWT.gif?ref_type=heads)

### Example 3 - Compliance Paticipant request <a name="example-3"></a>

- **Context**: Bob already has a LegalParticipant VC, a RegistratinNumber VC and a TermsAndCondition VC in his wallet. And he wants to become a Compliance Participant of the 'aster-x'.

- **Task performed by User**: BOB goes to the 'aster-x' ICP Portal, and selects the VC ComplianceParticipant. A presentation Qr Code appears on the screen and Bob scans it with his wallet, transmitting the VCs for LegalParticipant, RegistratinNumber and TermsAndCondition. Once BOB has transmitted his VCs and they are valid, another Qr Code is displayed. Bob then scans this Qr Code and receives the VC for ComplianceParticipant in his wallet.

- **Task performed by ICP**: ICP makes a request to the Gaia-x gateway to obtain the VC ComplianceParticipant's issuance information; The gateway replies to the ICP that a VP-request (Verifiable Presentation Request) must be made and transmits the Verifiable Definition (Presentation Exchange v2); The ICP constructs the presentation URI (OIDC4VP) and sends it to the portal, which displays the Qr Code; The ICP receives the VP sent by the BOB wallet; The ICP sends verifies the VP (signature and credentialStatus) and sends it to the gateway; The gateway requests Compliance from 'aster-x', including the VP; Compliance issues a ComplianceParticipant VC and sends it to the gateway; The gateway sends this VC to the ICP; The ICP creates an issuance uri (OIDC4VCI protocol); When bob scans the Qr Code, the ICP signs the VC and transmits it to the BOB wallet (OIDC4VCI protocol).

**This example use the Walt.id wallet**

![ComplianceParticipantGif](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/credentialissuer/-/raw/main/documentation/ComplianceParticipant.gif?ref_type=heads)

### Example 4 - Revocation of a Compliance Participant VC <a name="example-4"></a>

- **Context**: The Company Dufourstorage does not respect the conditions of Gaia-x and/or the federation, and 'aster-x' wants to revoke its VC of ComplianceParticipant.

- **Task performed by User**: The 'aster-x' administrator connects to the AdminPortal; Searches for the VC ComplianceParticipant of Company Dufourstorage and suspends/revokes him.

- **Task performed by ICP**: The AdminPortal redirects the 'aster-x' administrator to keycloak to log in; The ICP sends the VCs it has issued
  to the AdminPortal; The AdminPortal sends a request to the ICP specifying the VC and its new status (suspended or reactivated); The ICP sends a request to the revocation registry to update the VC's status (Bitstring Status List v1.0 Protocol).

![AdminPortalGif](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/credentialissuer/-/raw/main/documentation/RevocationComplianceParticipant.gif?ref_type=heads)

# Overview

## The Role of the Portal component

The portal is an essential part of our solution. It acts as a gateway between the user and the various departments involved in issuing and managing verifiable credentials.

The portal is a single-page website that interfaces directly with the user. It presents the user with a list of recoverable credentials, obtained by a call to the ICP and gateway. This is the Portal home page, which contains the list of VCs that ICP is capable of issuing.

![PortalHomePage](/documentation/PortalHomePage.png)

The portal is deployed by provider and by federation. In our current scenario, several types of VC are implemented (other VC types are currently being added) :

- **LegalParticipant** (Provider): This VC is proof of the legal existence of your entity or organization as a legal participant.
- **RegistrationNumber** (Federation): This VC represents a Participant's Legal Registration Number.
- **TermsAndCondition** (Provider): This VC represents Gaia's terms and conditions.
- **EmployeeCredential** (Provider): This VC represents a Gaia-X participant's proof of compliance.
- **ComplianceParticipant** (Federation): VC of the Compliance for a Gaia-x Participant
- **GrantedLabel** (Federation): This VC represents the Labelling level of a service.
- **ContractCredential** (Provider): This VC represents an ODRL contract signed between several Gaia-X participants.

Depending on the type of VC selected, the user is redirected to a different portal page.

If a form is to be filled in, the user is redirected to **/form** (this is the case for the VCs of LegalParticipant and TermsAndCondition).
Below is an example of the VC LegalParticipant form. The user must fill in the fields relating to his company, such as **Legal Name** or **Vat ID**.

![FormPage](/documentation/FormPage.png)

In the case of a presentation (as for RegistrionNumber and ComplianceParticipant VCs), the page is **/presentation**. Here's an example of a presentation page to obtain a RegistrationNumber VC, where users scan the QrCode and share their LegalParticipant VC.

![PresentationPage](/documentation/PresentationPage.png)

And in the case of issuance via a keycloak connection, the user is redirected to the keycloak url (url obtained with a call from the backOffice to the gateway). This is a simple connection to keycloak with username and password.

Finally, in all cases, the user is redirected to the portal's **/issuance** page, where a Qr Code containing the issuance openid URI (URL obtained via ICP) is displayed.
Below is an example of a VC issuance page containing the Qr Code to be scanned. Note the difference in the text above the Qr Code between the issuance page **/issuance** and the presentation page **/presentation**.

![IssuancePage](/documentation/IssuancePage.png)

## The Role of the CredentialIssuer component

The CredentialIssuer component is the ICP's backend. It is responsible for building, signing and storing the VCs it issues. It implements all the routes of the OID4VCI and OID4VP protocols, as well as the routes needed for the issuance and revocation processes required by the portal and admin portal. It also liaises with the Gaia-x gateway, which is responsible for making requests to the various components. 
This component is generic and totally independent of the Gaia-X ecosystem. It is linked to it via the Gaia-X Gateway.

## The Role of the Gateway Gaia-X component

The role of the Gaia-X Gateway is to make calls to the various components of the Gaia-x ecosystem. We have implemented this architecture so that the ICP component is Generic. In other words, it works with Gaia-X, but it could also work with another ecosystem. This is made possible by the plugin system implemented in the Gateway (a plugin represents a type of VC). 
This is explained in detail in the section [Gateway architecture](#gateway-gaia-x), and also in the section [Add a plugin for a new type of VC to be issued](#add-a-plugin-for-a-new-type-of-vc-to-be-issued), which explains how to easily add a new plugin, i.e. a new type of VC to be issued.

## The Role of Administrator Portal component

The Admin Portal is an essential tool for managing the VC lifecycle. This tool is also deployed by provider and federation. This means that the VCs in the AdminPortal are only those that have been issued by the ICP of the same tenant.

Users of the AdminPortal can search all the verifiable credentials that have been issued. At this stage, the queries are in mongoDB format for simplicity. Once the user has located the VC he wishes to modify, he has the choice of suspending or reactivating it. AdminPortal then sends the VC's ID and new status to the ICP. Finally, the ICP makes a request to the revocation register to update the VC's status.

Once a VC is suspended, it can no longer be used by an ICP VP-request. This is because when the ICP receives the Verifiable Presentation, it performs a signature check on the VCs, as well as a credententialStatus check on the VCs.

Here's an image of the admin Portal containing all the VCs that have been issued.

![AdminPortal](/documentation/AdminPortal.png)

And here's the diagram showing how Admin Portal works.

```mermaid
sequenceDiagram
participant AdminPortal as Admin Portal
participant ICP as Credential Issuer
participant Keycloak as Admin Keycloak
participant Registry as Revocation Registry
AdminPortal->>Keycloak: Login (redirect to Admin Keycloak)
Keycloak-->>ICP: Successful Authentication
ICP-->>AdminPortal: Send all credential issued by the ICP (paginate with 50 VC by default)
AdminPortal->>ICP: Request to suspense VC by id
ICP->>ICP: Find the VC by id in database and extract credentialStatus
ICP->>Registry: Send Request to update the revocation list
Registry-->>ICP: Send result
ICP->>AdminPortal: Send The VC with status updated
```

# Architecture

## OID4VCI

ICP implements the [OIDC4VCI draft 13](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html) protocol. Here are its specifications for the two output modes supported, with the main endpoints:

### Pre-Authorized workflow

```mermaid
sequenceDiagram
participant  User
participant  Wallet
participant  ICP
User->>ICP: Request a credential
ICP->>ICP: Generate new Pre-Authorized worklow with openid issuance URI
ICP-->>User: Send the openid issuance URI
User->>Wallet: User scan Qr Code (openid URI) with the wallet
Wallet->>ICP: Get the metadata /.well-known/openid-configuration of the Issuer
ICP-->>Wallet: Send Result
Wallet->>Wallet: Build the OIDC4VCI Token request
Wallet->>ICP: Send Token request to the endpoint (retrieved with metadata), including pre-authorized code
ICP->>ICP: Check the Token request and pre-authorized code
ICP->>ICP: Generate access_token
ICP-->>Wallet: Send the access_token
Wallet->>Wallet: Build the Credential request (including jwt with nonce send by the issuer)
Wallet->>ICP: Send the Credential request and the access_token
ICP->>ICP: Verify the access_token and Credential request (JWT signature)
ICP->>ICP: Build and sign the Verifiable Credential
ICP->>ICP: Build the Credential response (including the VC)
ICP-->>Wallet: Send the Credential response to the wallet
Wallet->>Wallet: Extract the VC from the response and store it
Wallet-->>User : Show the credential (VC) to the User 
```

- [Credential Offer endpoint](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html#name-credential-offer-endpoint): This endpoint is responsible for transmitting the available Verifiable Credentials to the user. Our Credential Issuer Component manages this route and its associated logic.
- [Token endpoint](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html#name-token-endpoint): This endpoint authenticates the user and returns the authorization token.
- [Credential Endpoint](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html#name-credential-endpoint) accepts the authorization token and proofs to construct the VC, then sends it back to the user. We use our Credential Issuer Component to manage this route and its associated logic.

### Authorized workflow
```mermaid
sequenceDiagram
participant  User
participant  Wallet
participant  ICP
User->>Wallet: User scan Qr Code (openid URI) with the wallet
Wallet->>ICP: Get the metadata /.well-known/openid-configuration of the Issuer
ICP-->>Wallet: Send Result
Wallet->>ICP: Send the Pushed Authorization Request (with pkce-challenge)
Wallet->>ICP: Send Authorization Request
ICP-->>User: Send the redirect_uri of portal
User->>ICP: Login in ICP
ICP-->>User: Login OK
User->>Wallet: Redirect in Wallet
Wallet->>ICP: Send the token request
ICP->>ICP: Check the Token request and resolve pkce-challenge
ICP->>ICP: Generate access_token
ICP-->>Wallet: Send the access_token
Wallet->>Wallet: Build the Credential request (including jwt with nonce send by the issuer)
Wallet->>ICP: Send the Credential request and the access_token
ICP->>ICP: Verify the access_token and Credential request (JWT signature)
ICP->>ICP: Build and sign the Verifiable Credential
ICP->>ICP: Build the Credential response (including the VC)
ICP-->>Wallet: Send the Credential response to the wallet
Wallet->>Wallet: Extract the VC from the response and store it
Wallet-->>User : Show the credential (VC) to the User
```

- [Credential Offer endpoint](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html#name-credential-offer-endpoint): This endpoint is responsible for transmitting the available Verifiable Credentials to the user. Our Credential Issuer Component manages this route and its associated logic.
- [Authorization Request](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html#name-authorization-request): This endpoint is used to request access authorisation. The authorisation request is in OAuth 2.0 format.
- [Pushed Authorization Request](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html#name-pushed-authorization-reques): This endpoint is used to push the authorisation request directly to the issuer. Use of this endpoint is optional but highly recommended as it provides greater security.
- [Token endpoint](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html#name-token-endpoint): This endpoint authenticates the user and returns the authorization token.
- [Credential Endpoint](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html#name-credential-endpoint): This endpoint accepts the authorization token and proofs to construct the VC, then sends it back to the user. We use our Credential Issuer Component to manage this route and its associated logic.

The implementation of draft 13 OID4VCI can be found in the file [src/controllers/oidc4VciController.js](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/credentialissuer/-/blob/main/src/controllers/oidc4VciController.js?ref_type=heads)

### Batch Credential

We also implemented the batch credential in accordance with the OID4VCI draft 13 specification. This involved modifying the `credential` endpoint in order to identify the format and definition of the credential in the body of the incoming request (`vct` in the case of an SD-JWT, or `type` in the case of other formats). When the wallet makes a request to the credential endpoint (in order to retrieve the credential), it must specify in the request body which credential it wishes to retrieve.

## OID4VP

ICP implements the [OIDC4VP](https://openid.net/specs/openid-4-verifiable-presentations-1_0.html) protocol. Here are its specifications, with the main endpoints.

```mermaid
sequenceDiagram
participant  User
participant  Wallet
participant  ICP
User->>Wallet: User scan Qr Code (openid URI) with the wallet
Wallet->>ICP: Get Authorization object with request_uri parameter
ICP->>ICP: Build and sign the Authorization object (jwt)
ICP-->>Wallet: Send Authorization object
Wallet->>Wallet: Shows the credentials corresponding to the presentation definition
User->>Wallet: User selects the credentials to be sent
Wallet->>Wallet: Build vp_token
Wallet->>ICP: Send vp_token to the response_uri(contained in the Authorization object)
ICP->>ICP: Check the vp_token (signature, expiration date, revocation)
ICP->>User: Shows the result to the user
```

- [Authorization Request](https://openid.net/specs/openid-4-verifiable-presentations-1_0.html#name-authorization-request): This endpoint returns the authorisation object for the presentation. In particular, this object contains the presentation definition (Presentation Exchange v2 format), the client_id_scheme (`kid` and `x509_san_dns` supported), and other metadata required for the wallet.
- [Callback Authorization (Response)](https://openid.net/specs/openid-4-verifiable-presentations-1_0.html#name-response): This endpoint is the callback for the wallet to send the Verifiable Presentation. The body of this request contains the `vp_token` (VP) and the state of the presentation.

## Rest API

The ICP provides the following routes : 

- {icp} : The url of the ICP.
- {gateway} : The name of the gateway we want to use
- {oid4vci_version}: The version of OID4VCI (oid4vci_draft8 or oid4vci_draft13)
- {vcType} : The type of VC (LegalPersonCredential, GrantedLabel, etc..)
- {id} : id of VC

### OIDC Route

The following OIDC routes are those of the standard OID4VCI and OID4VP protocols, which have been implemented.

| HTTP Verb | url                                                                               | input                                      | output                                       | Description                                                                                        |
| --------- | --------------------------------------------------------------------------------- | ------------------------------------------ | -------------------------------------------- | -------------------------------------------------------------------------------------------------- |
| HTTP GET  | https://{icp}/{gateway}/issuer/{oid4vci_version}/.well-known/openid-configuration | n/a                                        | The Json openid configuration                | ICP send openid-configuration json file. This file contains the issuer's metadata, with endpoints. |
| HTTP GET  | https://{icp}/{gateway}/issuer/{oid4vci_version}/.well-known/did.json             | n/a                                        | The DID doc of the issuer                    | ICP send the DID doc. This file contains the public key of the issuer for verify VC                |
| HTTP POST | https://{icp}/{gateway}/issuer/{oid4vci_version}/token                            | The Token request build by the wallet      | The access_token                             | This is the OIDC4VCI token endpoint                                                                |
| HTTP POST | https://{icp}/{gateway}/issuer/{oid4vci_version}/credential                       | The Credential request build by the wallet | The request response including the VC signed | This is the OIDC4VCI Credential endpoint                                                           |

### Issuance Route

The following routes are used to manage the issue process between the portal and the ICP. This is our own architecture, not a standard protocol.

| HTTP Verb | url                                       | input                                                                                     | output                                                                                 | Description                                                                                                                                                 |
| --------- | ----------------------------------------- | ----------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| HTTP GET  | https://{icp}/{gateway}/issuance          | n/a                                                                                       | A list of available Credentials                                                        | The gateway send the list of available Credentials for by provider                                                                                          |
| HTTP GET  | https://{icp}/{gateway}/issuance/{vcType} | n/a                                                                                       | The data used to start the issuance (form, presentation uri, or keycloak redirect uri) | The gateway send the type of issuance (form, keycloak, vp) and the associated data                                                                          |
| HTTP POST | https://{icp}/{gateway}/issuance/{vcType} | The data for continue issuance (completed form, verifiable presentation, or jwt keycloak) | The openid uri to receive the VC                                                       | The portal returns the information provided by the user to continue the issuance. This information is different (form, jwt, vp) depending on the type of VC |

### Issuance Route by unique link

These routes are similar to the previous issuance routes, except that they are used to enable issuance with a unique HTTPS link (which redirects to our portal). 

This is notably used in the contract negotiation workflow, where when the signing process is complete, all signatories receive an e-mail containing a unique HTTPS link enabling them to retrieve their contract VCs.
| HTTP Verb | url | input | output | Description |
|---|---|---|---|---|
| HTTP GET | https://{icp}/{gateway}/issuance/{vcType}?issuancebyurl=true | The data of VC | The unique url link | The gateway valid the data of VC and send to ICP. ICP issue the VC and retur the unique link HTTPS for continue issuance |
| HTTP POST | https://{icp}/{gateway}/issuance/{vcType}?id={id} | The data for continue issuance (VP) | The openid uri to receive the VC | The portal return the openidUrl of the vc issued on route previous route |

### Revocation Route

The following routes are used to manage the VC lifecycle between the administrator portal and the ICP. This is our own architecture for linking the front admin portal to the back ICP. 

With regard to the standard Bitstring Status List v1.0 protocol routes, our ICP does not implement them, and calls on our revocation register, which does implement them. The revocation register and its documentation can be found [here](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/revocation-registry).

| HTTP Verb | url                            | input                            | output                           | Description                                                                                               |
| --------- | ------------------------------ | -------------------------------- | -------------------------------- | --------------------------------------------------------------------------------------------------------- |
| HTTP GET  | https://{icp}/revocations?     | The query to filter the database | The all VC matched by the filter | Search route by query : "filter", "sort", "limit", "offset" (More details below at "Query for Search VC") |
| HTTP POST | https://{icp}/revocations/{id} | The action to execute            | Confirmation message             | Used for revocate, supenses, or reactivate VC (More details below at "Query for update status of VC")     |

#### Query for Search VC

Example of query used by Admin Portal for search VC : 

```
?offset=0&
limit=10&
sort={"id": "1"}&
filter={"credentialSubject.name": "Alice"}
```

Note: The above parameters must be encoded in URL format.

#### Query for update status of VC

Example of query used by Admin Portal for update status of VC :

```
POST https://{icp}/revocations/{id}

With body:
{
    "newStatus": "suspended"
}
```

The "newStatus" property in the body can take two values:

- **suspended**: To suspend the VC
- **alive**: To reactivate the VC

## Credential Issuer

The Credential Issuer component is the main component of the ICP, acting as the backend for the user portal and the administrator portal. This component contains all the architecture needed to manage several versions of the OID4VCI and OID4VP protocols using a plugin system. The implemented versions of OID4VCI and OID4VP can be found in the [src/services/oid4vci](#https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/credentialissuer/-/tree/main/src/services/oid4vci?ref_type=heads) and [src/services/oid4vp](#https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/credentialissuer/-/tree/main/src/services/oid4vp?ref_type=heads) folders respectively.
  
The logic for building and signing credentials is also present and is divided into several modules. Each module corresponds to a type of format. The credentials are signed using libraries depending on the format:
- ldp_vc format: [Gaia-X JSON Web Signature 2020](#https://gitlab.com/gaia-x/lab/json-web-signature-2020)
- SD-JWT format: [Meeco SD-JWT-VC](#https://github.com/Meeco/sd-jwt-vc)
- jwt_vc_json-ld format: [Jose](#https://www.npmjs.com/package/jose)

## Gateway Gaia-X

The Gateway's architecture has been designed so that new VCs can be easily added to issuer. This is made possible by a system of plugins that are loaded at application startup (one plugin represents one type of VC).

- Firstly, the Gateway implements a **GET /issuance** route which returns a list of VCs that the Gateway is able to issue. This list is retrieved via the configuration file **src/config/default-vc-configuration.json**. This file contains an array of objects, where each object contains the VC configuration to be issued. In addition, this file can be updated via the **PUT /config/credentials** route.
  Here are the properties of a VC object:
  - **name**: This is the name of the VC.
  - **type**: This is the type of VC issuance (form, presentation, keycloak).
  - **form** (if type is form): This parameter is only present if parameter **type** is **form**. This parameter contains a JSON object representing the form to be displayed to the user. This form must respect the JSONSchema of the [react-jsonschema-form](https://github.com/rjsf-team/react-jsonschema-form) library.
  - **presentation** (if type is presentaion) This parameter is only present if parameter **type** is **presentation**. This parameter contains the verifiable JSON-format definition of the VCs to be shared by the user.
  - **issuer**: This is a table containing the names of providers and/or federations that can issue the VC.
  - **fieldsToExtract**:  This is an object array containing the name of a field and its associated JsonPATH to enable simple extraction of a field in the VC in question. This parameter is optional.

Below is an example of a configuration file:

```json
[
    {
        "name": "RegistrationNumber",
        "type": "presentation",
        "presentation": {
            "format":null,
            "id":"1",
            "input_descriptors":[
                {
                    "constraints": null,
                    "format": null,
                    "group": null,
                    "id": "1",
                    "name": null,
                    "purpose": null,
                    "schema": {
                      "uri": "https://registry.aster-x.demo23.gxfs.fr/api/trusted-schemas-registry/v2/schemas/LegalParticipantSchema"
                    }
                }
            ],
            "name": null,
            "purpose": null,
            "submission_requirements": null
        },
        "fieldsToExtract" : [
            { "name": "leiCode", "jpath": "credentialSubject[gx:leiCode]" },
            { "name": "vatID", "jpath": "credentialSubject[gx:vatID]" },
            { "name": "EORI", "jpath": "credentialSubject[gx:EORI]" }
        ],
        "issuer": ["aster-x", "aster-x", "localhost"]
    },
    {
        "name": "EmployeeCredential",
        "type": "keycloak",
        "fieldsToExtract" : [
            { "name": "name", "jpath": "credentialSubject[name]" },
            { "name": "surname", "jpath": "credentialSubject[surname]" },
            { "name": "email", "jpath": "credentialSubject[email]" }
        ],
        "issuer": ["dufourstorage", "montblanc-iot", "localhost"]
    },
    {
        "name": "TermsAndConditions",
        "type": "form",
        "form": {
            "title": "Sign Gaia-X Terms And Conditions",
            "type": "object",
            "properties": {
              "text": {
                "type": "null",
                "title": "Gaiax",
                "description": "The PARTICIPANT signing the Self-Description agrees the following conditions: First, update its descriptions about any changes, be it technical, organizational, or legal - especially but not limited to contractual in regards to the indicated attributes present in the descriptions. Secondly, the keypair used to sign Verifiable Credentials will be revoked where Gaia-X Association becomes aware of any inaccurate statements in regards to the claims which result in a non-compliance with the Trust Framework and policy rules defined in the Policy Rules and Labelling Document (PRLD)."
              },
              "accept_conditions": {
                "type": "boolean",
                "default": false,
                "description": "By checking this box, you agree to the above terms and conditions.",
                "title": "Accept terms and conditions"
              }
            }
        },
        "fieldsToExtract" : [
        ],
        "issuer": ["dufourstorage", "montblanc-iot", "localhost"]
    }
]
```

- Then, for each VC in the configuration file, a javascript file with the name {vcName}.js must be placed in the plugins directory. This file must have the same name as the **name** field in the previous configuration file in order to make the connection. This file will also be automatically loaded when the gateway is restarted. This file must contain at least two mandatory functions:
  
  - **startIssuance** : This function takes no parameters and returns the VC configuration JSON object.
  - **continueIssuance**: This function takes a **data** parameter which can contain a Verifiable Presentation containing the VCs the user has shared, the form data filled in by the user, or a JWT keycloak containing the user's information. Depending on the type of VC. The function then processes the data received as parameters, calling Gaia-X components such as Compliance and RegistratonNumber. Once the data has been processed, the function returns the new VC data to be issued. The return format is a JSON object of the form: ```{ credential: {object}, credential_type: {string}}``` Where **credential** is an object containing the VC data (credentialSubject, evidence, etc.), and **credential_type** is a string containing the VC type.

- Finally, the Gateway implements the **POST /issuance/{vcName}** route, which is used to issue a VC. The {vcName} parameter in the route must also have the same name as the plugin and VC name in the configuration file, as explained above. This route does two things: 
  
  - Start an issuance: the data returned by this route is that of the **startIssuance** function of the plugin concerned (plugin **{vcName}**). The data returned is therefore, for example, a form or a veriafiable presentation. This route is called by the ICP with this object in body: ```{ action: "startIssuance" }```.
  - ContinueIssuance: The data returned is that of the **continueIssuance** function (which contains the VC data). This route is called by the ICP with this object in body: ```{ action: "continueIssuance", {type}: {object} }``` where the **type** property can have the values **form**, **jwt**, or **presentation**.

## Types of issuance

### Form Issuance

- Form issuance is used to receive VCs that require user input (such as LegalParticipant).
- The portal requests the form for the chosen VC from the ICP, which in turn requests it from the Gaiax gateway.
- The Gaiax gateway returns the form in JSON format to the ICP and then to the Portal. 
- The Portal displays the form in HTML.
- Once the user has completed the form, it is sent to the gateway, which validates the data.
- The ICP start a new Pre-Authorized workflow with the data, create the openid issuance URI and send it to the portal.
- The user scans the QR code on the portal and starts the issuance process, at the end of which they receive their signed VC.

The diagram below shows the form issuance:

```mermaid
sequenceDiagram
participant  Portal
participant  CredentialIssuer  as  Credential  Issuer
participant  Gateway  as  Gateway Gaiax
participant  Wallet
Portal->>CredentialIssuer: Get the list of Verifiable Credentials
CredentialIssuer->>Gateway: Get the list of Verifiable Credentials
Gateway-->>CredentialIssuer: Send the list of Verifiable Credentials
CredentialIssuer-->>Portal: Send the list of Verifiable Credentials
Portal->>CredentialIssuer: Start the issuance type form (such as LegalPerson)
CredentialIssuer->>Gateway: Get the form
Gateway-->>CredentialIssuer: Send the form
CredentialIssuer-->>Portal: Send the form
Portal->>Portal: User fills in the form
Portal->>CredentialIssuer: Send completed form
CredentialIssuer->>Gateway: Send completed form
Gateway->>Gateway: Check The form
Gateway-->>CredentialIssuer: Return Ok with the VC containing form data
CredentialIssuer->>CredentialIssuer: Create new Pre-Authorize workflow (includes issuance uri generation)
CredentialIssuer-->>  Portal  : Sends the URI.
Portal->>  Wallet  : User scans the QR code and accept the offer
Wallet-->>CredentialIssuer  : Transmits user's acceptation.
CredentialIssuer ->>  Wallet  : Sends the finalized VC to user's wallet.
```

### Presentation Issuance

- Issuance by presentation is used to receive VCs who need to present one or more other VCs (presentation of LegalParticipant for RegistrationNumber, and presentation of ServiceOffering and SelfAssessedComplianceCriteriaClaim for GrantedLabel (labelling)).
- The gateway asks the ICP for the openid presentation URI. 
- The ICP asks the gateway for the verifiable definition required for the presentation, then starts a new presentation process (with the creation of a presentation URI) based on this information.
- The user scans the presentation Qr Code with his wallet and agrees to share the requested VCs. 
- Once the user has scanned the QR code, the wallet sends the verifiable presentation to the ICP. 
- The ICP validates the verifiable presentation, performing a signature and credentialStatus check for each VC in the VP. 
- The ICP then sends the verifiable presentation to the Gaia-X gateway, specifying the desired service (RegistrationNumber, Compliance, etc.). 
- The gateway makes a request to the Gaia-X component in question, then sends the new VC information back to the ICP, which stores it in mongodb. 
- At the same time, the gateway pings the ICP to check whether the user has scanned the Qr code. 
- When the scan is complete, ICP start a new Pre-Authorized workflow with the data stored in mogodb previously and create the openid issuance URI. 
- ICP sends the openid issuance URI to the portal, and the user scans the QR code on the portal to retrieve the new VC.

The diagram below shows the presentation issuance:

```mermaid
sequenceDiagram
participant  Portal
participant  CredentialIssuer  as  Credential  Issuer
participant  Gateway  as  Gateway Gaiax
participant  Wallet
Portal->>CredentialIssuer: Get the list of Verifiable Credentials
CredentialIssuer->>Gateway: Get the list of Verifiable Credentials
Gateway-->>CredentialIssuer: Send the list of Verifiable Credentials
CredentialIssuer-->>Portal: Send the list of Verifiable Credentials
Portal->>CredentialIssuer: Start the issuance type presenation (such as GrantedLabel or ParticipantCredential)
CredentialIssuer->>Gateway: Get the presentation of VC
Gateway-->>CredentialIssuer: Send the presentation of VC
CredentialIssuer->>CredentialIssuer: Build new Presentation request workflow (generation presentation URI)
CredentialIssuer-->>Portal: Send the presentation URI
Portal->> Wallet  : User scans the QR code and accept to share the VC (LegalPerson in case of ParticipantCredential)
loop Waiting for scan user
    Portal->>+CredentialIssuer: Ping to check if scan is OK
end
Wallet-->>CredentialIssuer : Send the VP
CredentialIssuer->>CredentialIssuer: Receive VP, check signature and credentialStatus for all VC
CredentialIssuer->>Gateway: Send the VP to the Gateway
Gateway->>Gateway: Build data of new VC (call Labelling if GrantedLabel)
Gateway-->>CredentialIssuer: Send the data of new VC
CredentialIssuer->>CredentialIssuer: Create new Pre-Authorize workflow (includes issuance uri generation)
CredentialIssuer-->> Portal  : Loop scan is Done and send the issuance URI
Portal->>  Wallet  : User scans the QR code and accept the offer
Wallet-->>CredentialIssuer  : Transmits user's acceptation.
CredentialIssuer ->>  Wallet  : Sends the finalized VC to user's wallet.
```

### Keycloak issuance

- Keykloac issuance is used to receive VCs that require the user to connect to a back office (such as keycloak for EmployeeCredential).
- The portal requests the keycloak url from the ICP, which in turn requests it from the Gaiax gateway.
- The Gaiax gateway sends the url back to the ICP and then to the Portal. The Portal redirects the user to the Back Office keycloak. 
- The user connects to the keycloak and is redirected to the portal with his BackOffice data, as well as an authorization token and a proof. All this data is sent to the ICP, which checks the poof and start a new Pre-Authorized workflow (create the openid issuance URI). The ICP sends the Portal the openid issuance URI. 
- The user scans the QR code on the portal and starts the issuance process, at the end of which they receive their signed VC.

The diagram below shows the keycloak issuance:

```mermaid
sequenceDiagram
participant  Portal
participant  CredentialIssuer  as  Credential  Issuer
participant  Gateway  as  Gateway Gaiax
participant  Keycloak
participant  Wallet
Portal->>CredentialIssuer: Get the list of Verifiable Credentials
CredentialIssuer->>Gateway: Get the list of Verifiable Credentials
Gateway-->>CredentialIssuer: Send the list of Verifiable Credentials
CredentialIssuer-->>Portal: Send the list of Verifiable Credentials
Portal->>CredentialIssuer: Start the issuance type keycloak (such as Employee)
CredentialIssuer->>Gateway: Get the keycloak url
Gateway-->>CredentialIssuer: Send the keycloak url
CredentialIssuer-->>Portal: Send the keycloak url
Portal->>Keycloak: Redirect and connexion to Keycloak
Keycloak-->>Portal: Successful connection and return Auth token and User Data
Portal->>CredentialIssuer: Continue Issuance with Auth token and proofs
CredentialIssuer->>CredentialIssuer: Check auth Token
CredentialIssuer->>Gateway: Send the JWT user data
Gateway->>Gateway: Build data of new VC (Such as EmployeeCredential)
Gateway-->>CredentialIssuer: Send the data of new VC
CredentialIssuer->>CredentialIssuer: Create new Pre-Authorize workflow (includes issuance uri generation)
CredentialIssuer-->> Portal  : Send the issuance URI
Portal->>  Wallet  : User scans the QR code and accept the offer
Wallet-->>CredentialIssuer  : Transmits user's acceptation.
CredentialIssuer ->>  Wallet  : Sends the finalized VC to user's wallet.
```

# Add a plugin for a new type of VC to be issued

Each type of VC issued by ICP is represented by a plugin in the gateway.
The steps below describe the installation and configuration of a new VC type:

- In the gateway's VC configuration file, add a new object representing the new VC to be issued:
  Below is an example for the **MyVC** VC with the associated VerifiableDefinition **MyVerifiableDefinition** and issuers
  
  ```json
  {
      "name": "MyVC",
      "type": "presentation",
      "presentation": { "MyVerifiableDefinition" },
      "issuer": ["aster-x", "aster-x", "localhost"]
  }
  ```
- Create a javascript file (extension **.js**) with the same name as the **name** parameter above, followed by the **.js** extension : **MyVC.js**
- In this file, create the :
  - **startIssuance** (See [Gateway Gaia-X](#gateway-gaia-x) for more details)
  - **continueIssuance** (See [Gateway Gaia-X](#gateway-gaia-x) for more details)
- Restart the Gaia-X Gateway components, and your ICP will be able to deliver the new VC.

# Technologies and tools used

- Node.js
- ReactJS
- OID4VCI, OID4VP, and Bitstring Status List v1.0 protocols
- Keycloak
- Mongodb
- Wallet (Walt.id, Talao, Sphereon)

# Installation

## Setting environment variables

### CredentialIssuer environment variables

- BASE_URL : The Internet-accessible URL of credential Issuer component (NGROK recommended for local use).
    Use NGROK : ```ngrok http http://localhost:3002```
- PRIVATE_KEY : The private key in JWK format. Example:
  
  ```json
  {
    "crv": "Ed25519",
    "d": "tko2H4mjQotWOaKLOIx4VWHPipCo2rn3qLld5LwiL3I",
    "x": "Jgs9_z5iZ8Bfze-iyHIg2o2IB38tkEVAfTWFUKd9Id0",
    "kty": "OKP",
    "kid": "c87e2806-d2f7-47f8-b03f-bba8fb19932d"
  }
  ```
  
  Note: The private key must have a kid property, which is a unique identifier of your choice
- PORTAL_URL: The portal URL (default http://localhost:3000)
- API_KEY_REVOCATION_REGISTRY: API Key of registry revocation (Use : 109353c6-6432-4acf-8e77-ef842f64a664)

### Gateway Gaiax environment variables

- API_KEY_AUTHORIZED : The api key of congig route

### Portal environment variables

- REACT_APP_ICP_URL : The URL of credentialIssuer component (default http://localhost:3002/gaiax)
- REACT_APP_KEYCLOAK_URL : The URL of Keycloak (used for Employee Credential, default https://keycloak.demo23.gxfs.fr/realms/icp/protocol/openid-connect/token)

## For develpment

!! You must also have mongodb running on your machine on port 27017.

To install ICP locally, the  components Portal, AdminPortal, CredentialIssuer, and Gaia-x Gateway must be running simultaneously. The links can be found below:

- [Portal](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/portal)
- [CredentialIssuer](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/credentialissuer)
- [Gateway Gaia-x](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/gatewaygaiax)
- [AdminPortal](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/adminportal)

To run the components : `npm install` and `npm start`

## For testing with docker

To make it easier to deploy the component locally, you can use the docker compose :

- Check that you have pulled the components and that the paths are correct in the docker-compose
  
  ```
  credentialIssuer:
   image: node:lts
   volumes:
     - ./:/usr/src/app             -> The path to the credentialIssuer component
   ...
  
  gaiaxGateway:
   image: node:lts
   volumes:
     - ../gatewaygaiax:/usr/src/app  -> The path to the gateway gaiax component
   ...
  
  portal:
   image: node:lts
   volumes:
     - ../portal/:/usr/src/app     -> The path to the portal component
   ...
  ```

- In docker compose, set the env var BASE_URL with the value of an Internet-accessible URL (NGROK recommended for local use).
  Note that the URL must not have a '/' at the end (https://ngrok_url.app is ok, https://ngrok_url.app/ is not ok) 
  
  ```
  credentialIssuer:
   image: node:lts
   volumes:
     - ./:/usr/src/app
   ...
   environment:
     ...
     BASE_URL: <https://ngrok_url.app>       -> Replace URL here
  ```

- Run `docker-compose up --build`

# Authors and acknowledgment

GXFS-FR

# License

The ICP is delivered under the terms of the Apache License Version 2.0.