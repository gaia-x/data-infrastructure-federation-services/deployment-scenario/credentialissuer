FROM node:alpine

RUN addgroup -S appgroup
RUN adduser -S appuser -G appgroup

WORKDIR /usr/src/app

ENV PATH usr/src/app/node_modules/.bin:$PATH

COPY ./config ./config
COPY ./src ./src

COPY package*.json .
COPY .env .

RUN npm install --silent --ignore-scripts

USER appuser

EXPOSE 3002
CMD ["npm", "start"]