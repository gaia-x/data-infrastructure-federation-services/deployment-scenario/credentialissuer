const axios = require('axios');
const logger = require('./logger');
const { v4: uuidv4 } = require('uuid');
const HttpError = require('@utils/httpError.js');
const { digest } = require('@sd-jwt/crypto-nodejs');
const { put, getOne, findMany } = require('./mongodb.js');
const { decodeSdJwt, getClaims } = require('@sd-jwt/decode');
const routingTable = require('@services/routingTableManager');
const { signJsonldCredential, signSdJWTCredential } = require('./signature');


// Headers for Revacation Registry
const statusEntryHeaders = {
    "Accept": "application/json",
    "Content-Type": "application/json",
    "X-API-KEY": process.env.API_KEY_REVOCATION_REGISTRY
};

// Get Status Entry by call Revocation Registry
async function getStatusEntry() {
    const statusEntryConfig = {
        "name": "default",
        "body": {
            "credentialUrl": routingTable.getConnectionUrl('revocationRegistry', 'credentials'),
            "purpose": "suspension"
        }
    };

    try {
        const url = routingTable.getConnectionUrl('revocationRegistry', 'statusEntry');
        const result = await axios.post(
            `${url}?issuerId=${statusEntryConfig.name}`,
            statusEntryConfig.body,
            statusEntryHeaders
        );
        return result.data;
    }
    catch (error) {
        throw new HttpError({ message: 'Error to get statusEntry', error_description: error.message, status_code: 500 });
    }
}


// Save Credential in mongodb
async function saveCredential(credential) {
    put('Credentials', { "id": credential.id, object: { "status": "alive", "credential": credential } });
}

// Get Credential by id
async function getOneCredential(id) {
    return await getOne('Credentials', id);
}


async function findManyCredential(request) {
    return findMany('Credentials', request)
}


/*
ancien status   |   nouveau     |    actions                |
actif           | actif         | return true               |
suspended       | suspended     | return true               | 
revoked         | revoked       | return true               |

revoked         | actif         | return false              | 
revoked         | suspended     | return false              |

actif           | revoked       | call registry and save    |
actif           | suspended     | call registry and save    | 
suspended       | actif         | call registry and save    | 
suspended       | revoked       | call registry and save    | 
*/
async function updateStatusCredential(id, newStatus) {
    let old = await getOneCredential(id);

    if (old.status == newStatus) return { "id": id, "status": newStatus };
    if (old.status == "revoked") {
        throw new HttpError({ message: 'Invalid_request', error_description: 'A revoked status can not be updated', status_code: 400 });
    }
    if ((old.status == 'alive') && (newStatus == 'revoked') ||
        (old.status == 'suspended') && (newStatus == 'revoked')) {
        const url = routingTable.getConnectionUrl('revocationRegistry', 'revoke');
        await axios.post(url, { "credentialStatus": old.credential.credentialStatus }, { headers: statusEntryHeaders }).then(response => {
            put('Credentials', { "id": id, object: { "status": newStatus, "credential": old.credential } });
        }).catch(error => {
            throw (error);
        });
        return { "id": id, "status": newStatus };
    }
    if ((old.status == "alive") && (newStatus == "suspended")) {
        const url = routingTable.getConnectionUrl('revocationRegistry', 'suspend');
        await axios.post(url, { "credentialStatus": old.credential.credentialStatus }, { headers: statusEntryHeaders }).then(response => {
            put('Credentials', { "id": id, object: { "status": newStatus, "credential": old.credential } });
        }).catch(error => {
            console.log(error);
            throw (error);
        });
        return { "id": id, "status": newStatus };
    }
    if ((old.status == "suspended") && (newStatus == "alive")) {
        const url = routingTable.getConnectionUrl('revocationRegistry', 'reactivate');
        await axios.post(url, { "credentialStatus": old.credential.credentialStatus }, { headers: statusEntryHeaders }).then(response => {
            put('Credentials', { "id": id, object: { "status": newStatus, "credential": old.credential } });
        }).catch(error => {
            throw (error);
        });
        return { "id": id, "status": newStatus };
    }
    throw new HttpError({ message: 'Error to updateStatusCredential', error_description: `Error with newStatus: ${newStatus} for credential ${id}`, status_code: 500 });
}


/**
 * Build the jsonld credential
 * @param {*} credential The credential to build and sign
 * @param {*} credentialType The credential type
 * @param {*} issuerDid The issuer did
 * @returns 
 */
async function buildJsonLdCredential(credential, credentialType, issuerDid) {
    try {
        let credentialToSign = { ...credential };

        const defaultContext = [
            "https://w3id.org/vc/status-list/2021/v1",
            "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/jws-2020",
            "https://schema-registry.aster-x.demo23.gxfs.fr/contexts/credentials"
            //"https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
        ];

        // ########## TODO: Investigate to fix format id for all credential ##########
        const id = `${process.env.BASE_URL}/${uuidv4()}/data.json`;
        credentialToSign["@context"] = credential["@context"] || defaultContext
        credentialToSign["type"] = credential.type || ["VerifiableCredential", credentialType];
        credentialToSign["id"] = credential.id || id;
        //credentialToSign["credentialSubject"]["id"] = issuerDid;
        credentialToSign["issuer"] = issuerDid;
        credentialToSign["issuanceDate"] = new Date().toISOString();
        credentialToSign["validFrom"] = new Date().toISOString();
        credentialToSign["expirationDate"] = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
        credentialToSign["validUntil"] = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toISOString();
        //credentialToSign["credentialStatus"] = await getStatusEntry();
        delete credentialToSign["proof"];

        return credentialToSign;
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
    }
}


/**
 * Build the main payload of the SD-JWT credential
 * @param {*} issuer The URL of the issuer
 * @param {*} vct The credential template
 * @param {*} holderPublicKey The holder public key
 * @returns 
 */
async function buildSdJwtPayload(issuer, vct, holderPublicKey) {
    try {
        const sdJWTPayload = {
            iat: Math.floor(Date.now() / 1000),
            exp: Math.floor(new Date(new Date().setFullYear(new Date().getFullYear() + 1)).getTime() / 1000),
            iss: issuer,
            vct: vct,
        };

        if (holderPublicKey !== undefined && holderPublicKey !== '') {
            sdJWTPayload.cnf = {
                jwk: holderPublicKey,
            }
        }

        return sdJWTPayload;
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
    }
}


/**
 * Build and sign a Credential
 * @param {*} credential The credential to sign
 * @param {*} credentialType The credential type
 * @param {*} issuerDid The issuer DID
 * @param {*} issuerUrl The url of the issuer
 * @param {*} issuerVerificationMethode The issuer verification method
 * @param {*} format The format of the credential: 'jwt_vc', 'vc+sd-jwt' or 'ldp_vc'
 * @param {*} holderPublicKey The holder public key
 * @returns 
 */
async function signCredential({ credential, credentialType, issuerDid, issuerUrl, issuerVerificationMethode, format, holderPublicKey }) {
    try {
        const privateKey = JSON.parse(process.env.PRIVATE_KEY);
        switch (format) {
            case "ldp_vc": {
                const jsonLdCredntial = await buildJsonLdCredential(credential, credentialType, issuerDid);
                return await signJsonldCredential(jsonLdCredntial, issuerVerificationMethode, privateKey);
            }
            case "vc+sd-jwt": {
                const sdJWTPayload = await buildSdJwtPayload(issuerUrl, credential.vct, holderPublicKey);
                const sdJWTHeader = { kid: privateKey.kid };
                return await signSdJWTCredential(sdJWTHeader, sdJWTPayload, credential.vcClaims, credential.sdVCClaimsDisclosureFrame, privateKey);
            }
            default:
                throw new HttpError({ message: 'Invalid_request', error_description: `Format not supported: ${format}`, status_code: 400 });
        }
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
    }
}


/**
 * This function verify the VerifiablePresentation with checkSignature and checkCredentialStatus for all credential
 * @param {*} vp The verifiable presentation to verify
 * @returns True if the VerifiablePresentation is valid (all credential valid), else throw an error
 */
async function verifyVerifiablePresentation(vp) {
    try {
        if (!vp.verifiableCredential || vp.verifiableCredential.length === 0) throw new Error('The verifiable presentation is not valid');

        const response = await axios.post(routingTable.getConnectionUrl('verifier', 'rules'), vp.verifiableCredential);
        if (response.status !== 200) {
            throw new HttpError({ message: 'Access_denied', error_description: 'One or more VCs are invalid', status_code: 401 });
        }

        logger.info("VerifiablePresentation Verified Successfully");
        return true;
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
    }
};


async function decodeSdJwtCredential(sdJWT) {
    try {
        const decodedSdJwt = await decodeSdJwt(sdJWT, digest);
        const claims = await getClaims(
            decodedSdJwt.jwt.payload,
            decodedSdJwt.disclosures,
            digest,
        );

        return { claims, decodedSdJwt };
    }
    catch (error) {
        throw new HttpError({ message: 'Invalid_request', error_description: `Error to decode SD-JWT: ${error.message}`, status_code: 400 });
    }
}


module.exports = {
    saveCredential,
    signCredential,
    getOneCredential,
    findManyCredential,
    decodeSdJwtCredential,
    updateStatusCredential,
    verifyVerifiablePresentation
}