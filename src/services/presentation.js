const axios = require('axios');
const HttpError = require('@utils/httpError.js');
const routingTable = require('@services/routingTableManager');

async function createNewPresentation({ gateway, presentation_options, issuance_options, presentation_definition_name }) {
    try {
        const gatewayURL = `${routingTable.getConnectionUrl(gateway, 'presentation_definitions')}/${presentation_definition_name}`;
        const presentation_definition = (await axios.get(gatewayURL)).data;
        const oid4vpModule = await import(`./oid4vp/${presentation_options.oid4vp_version}.js`);
        return await oid4vpModule["createNewPresentation"]({ gateway, presentation_options, issuance_options, presentation_definition });
    }
    catch (error) {
        if (error.isAxiosError && error.response && error.response.status != 500) {
            throw new HttpError({ message: error.response.data.error, status_code: error.response.status });
        }
        throw(error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
    }

}



module.exports = {
    createNewPresentation
}