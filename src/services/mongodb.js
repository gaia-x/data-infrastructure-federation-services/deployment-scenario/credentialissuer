const logger = require('./logger');
const { MongoClient } = require('mongodb');
const defaultClientMetadata = require('../../config/default-client-metadata-oid4vp.json');
const defaultOpenidConfiguration = require('../../config/default-openid-configuration.json');

let client;
let config = {
    "connectionString" : `mongodb://${process.env.MONGO_INITDB_ROOT_USERNAME}:${process.env.MONGO_INITDB_ROOT_PASSWORD}@${process.env.MONGO_SERVICE_HOST}:${process.env.MONGO_SERVICE_PORT}/`,
    "dbName": "CredentialsIssuer",
    "dbCollections": ["Credentials", "Configuration"],
    "defaultMongoDB": process.env.MONGO_DOCKER_URL || "mongodb://127.0.0.1:27017",
}

async function init() {
    try {
        client = new MongoClient((typeof process.env.MONGO_INITDB_ROOT_USERNAME != 'undefined') ? config.connectionString : config.defaultMongoDB);
        await client.connect();
        const db = client.db(config.dbName);
        logger.info(`Connected to mongodb`);
        for (const collectionName of config.dbCollections) {
            const collections = await db.listCollections({ name: collectionName }).toArray();
            if (collections.length === 0) {
                await db.createCollection(collectionName);
                console.log(`La collection ${collectionName} a été crée avec succès.`);
            }
        }
        await db.collection("Credentials").createIndex({ "id": 1 }, { unique: true });
        await db.collection("Credentials").createIndex({ "ts": 1 }, { unique: false });
        await put('Configuration', { id: 'client-metadata', object: defaultClientMetadata });
        await put('Configuration', { id: 'openid-configuration', object: defaultOpenidConfiguration });
    } catch (error) {
        console.error('Une erreur s\'est produite lors de la création de la collection:', error); 
        process.exit(1);
    }
}

async function put(collectionName, row) {
    await client.db(config.dbName).collection(collectionName).replaceOne({ "id" : row.id }, { "id" : row.id, "ts": new Date().toISOString(), "object": row.object }, { upsert: true });
}

async function deleteOne(collectionName, id) {
    await client.db(config.dbName).collection(collectionName).deleteOne({ "id": id });
}

async function getOne(collectionName, id) {
    const obj = await client.db(config.dbName).collection(collectionName).findOne({ "id" : id } );
    return (obj != null) ? obj.object : null;
}

async function findMany(collectionName, request) {
    const collection = client.db(config.dbName).collection(collectionName);
    let f = request.hasOwnProperty("filter") ? request.filter : { $expr: { $gt: [ { $strLenCP: "id" }, 1] } }
    const result = await collection
        .find(f)
        .skip(request.hasOwnProperty("offset") ? parseInt(request.offset) || 0 : 0)
        .limit(request.hasOwnProperty("limit") ? parseInt(request.limit) || 10 : 10)
        .sort((request.hasOwnProperty("sort")) ? request.sort : { "ts": "1" }).toArray();
    const numberOfCredentials = await collection.countDocuments(f);
    logger.info("Credential retrieve with success", {"Number of credentials": numberOfCredentials });
    return { count: numberOfCredentials, credentials: result.map(row => ({ "id": row.id, "ts" :row.ts, "status": row.object.status, "credential": row.object.credential })) };
}

module.exports = { init, put, deleteOne, getOne, findMany };