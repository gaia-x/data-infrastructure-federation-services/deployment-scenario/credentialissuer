const axios = require('axios');
const utils = require("@utils/utils");
const { v4: uuidv4 } = require('uuid');
const logger = require('@services/logger');
const { Resolver } = require('did-resolver');
const HttpError = require('@utils/httpError');
const { createPublicKey } = require('crypto');
const { getDidWeb } = require('@services/didDocument');
const routingTable = require('@services/routingTableManager');
const { put, getOne, deleteOne } = require('@services/mongodb');
const { decodeJwt, decodeProtectedHeader, exportJWK } = require('jose');
const { saveCredential, signCredential } = require('@services/credential');


/**
 * Create a new Pre Authorized Workflow
 * @param {*} body Object with issuance informations
 *      @example body = {
 *                  gateway: String - Gateway name
 *                  issuance_options: Object - Issuance options,
 *                      @example issuance_options = {
 *                          oid4vci_version: String - version of OID4VCI
 *                          oidc_prefix: String - prefix for the Openid URI
 *                          tx_code_required: String - if the user pin is required
 *                          tx_code: String - The user pin (if required)
 *                      }
 *                  credentials_information: Array - Credentials information
 *                      @example issuance_options = [{
 *                          format: String - The format of the credential
 *                          type: String - The type of the credential
 *                          credential: Object - The credential
 *                      }, ...]
 *               }
 * @returns The Openid URI
 */
async function createNewPreAuthorizedWorkflow({ gateway, issuance_options, credentials_information }) {
    try {
        // Create object for pre authorized workflow
        let dataCredentialPreAuthorization = {
            'credentials_information': credentials_information,
            'pre-authorized_code': uuidv4(),
            'tx_code_required': issuance_options.tx_code_required || false,
            'tx_code': issuance_options.tx_code,
            'nonce': uuidv4(),
        }

        // Save object in database
        await put('CredentialsRequest', { id: dataCredentialPreAuthorization['pre-authorized_code'], object: dataCredentialPreAuthorization });

        // Create Openid URI
        const oidcPrefix = issuance_options.oidc_prefix || 'openid-credential-offer://?';
        const credentialOffer = {
            credential_issuer: `${process.env.BASE_URL}/${gateway}/issuer/oid4vci_draft13`,
            credential_configuration_ids: credentials_information.map(item => item.type),
            grants: {
                'urn:ietf:params:oauth:grant-type:pre-authorized_code': {
                    'pre-authorized_code': dataCredentialPreAuthorization['pre-authorized_code']
                }
            }
        }

        // Add tx_code if required
        if (issuance_options.tx_code_required) {
            credentialOffer.grants['urn:ietf:params:oauth:grant-type:pre-authorized_code']['tx_code'] = {
                length: issuance_options.tx_code.length,
                input_mode: 'numeric',
                description: issuance_options.tx_code_message || 'Please enter the code'
            }
        }

        const encodedOidcUri = `${oidcPrefix}credential_offer=${encodeURI(JSON.stringify(credentialOffer))}`;
        logger.info('Openid Uri created', { 'openid-uri': encodedOidcUri, 'nonce': dataCredentialPreAuthorization.nonce });
        return encodedOidcUri;
    }
    catch (error) {
        throw new HttpError({ message: 'Error in createNewPreAuthorizedWorkflow', error_description: error.message, status_code: 500 });
    }
}


async function createNewAuthorizationCode({ gateway, issuance_options, credentials_information }) {
    try {
        // Create object for authorized workflow
        const dataAuthorizationCode = {
            credentials_information: credentials_information,
            issuer_state: uuidv4()
        }

        // Save object in database
        await put('CredentialsRequest', { id: dataAuthorizationCode.issuer_state, object: dataAuthorizationCode });

        // Create Openid URI
        const oidcPrefix = issuance_options.oidc_prefix || "openid-credential-offer://?";
        const credentialOffer = {
            credential_issuer: `${process.env.BASE_URL}/${gateway}/issuer/oid4vci_draft13`,
            credential_configuration_ids: credentials_information.map(item => item.type),
            grants: {
                'authorization_code': {
                    'issuer_state': dataAuthorizationCode.issuer_state
                }
            }
        }

        const encodedOidcUri = `${oidcPrefix}credential_offer=${encodeURI(JSON.stringify(credentialOffer))}`;
        logger.info('Openid Uri created', { 'openid-uri': encodedOidcUri });
        return encodedOidcUri;
    }
    catch (error) {
        throw new HttpError({ message: 'Error in createNewAuthorizationCode', error_description: error.message, status_code: 500 });
    }
}


async function jwks(req) {
    try {
        const privateJwk = JSON.parse(process.env.PRIVATE_KEY);
        const keyObject = createPublicKey({ key: privateJwk, format: 'jwk' });
        const publicKeyJwk = await exportJWK(keyObject);

        if (publicKeyJwk.kty === 'RSA') {
            publicKeyJwk.alg = privateJwk.alg || 'PS256';
        }
        if (privateJwk.kid !== undefined && privateJwk.kid !== '') publicKeyJwk.kid = privateJwk.kid;

        return {
            keys: [publicKeyJwk]
        }
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: 'Error in jwks', error_description: error.message, status_code: 500 }));
    }
}

async function getOpenidConfiguration(req) {
    try {
        const oidcConfiguration = await getOne('Configuration', 'openid-configuration');
        const gatewayUrl = routingTable.getConnectionUrl(req.params.gateway, 'credentials-supported');
        const response = await axios.get(gatewayUrl);
        const valuesToReplace = {
            BASE_URL: process.env.BASE_URL,
            GATEWAY_NAME: req.params.gateway,
            OID4VCI_VERSION: req.params.oid4vci_version
        };

        const replacedJson = utils.replaceValuesInJSON(oidcConfiguration, valuesToReplace);
        replacedJson.credential_configurations_supported = response.data.credential_configurations_supported;
        return replacedJson;
    } catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: 'Error to getOpenidConfiguration', error_description: error.message, status_code: 500 }));
    }
};


async function getOauthAuthorizationServer(req) {
    return {
        "issuer": process.env.BASE_URL,
        "authorization_endpoint": `${process.env.BASE_URL}/${req.params.gateway}/issuer/${req.params.oid4vci_version}/authorize`,
        "token_endpoint": `${process.env.BASE_URL}/${req.params.gateway}/issuer/${req.params.oid4vci_version}/token`,
        "response_types_supported": [
            "code"
        ],
        "grant_types_supported": [
            "authorization_code"
        ],
        "pushed_authorization_request_endpoint": `${process.env.BASE_URL}/${req.params.gateway}/issuer/${req.params.oid4vci_version}/authorize/par`,
        "require_pushed_authorization_requests": true,
        "dpop_signing_alg_values_supported": [
            "ES256"
        ]
    }
}


async function par(req) {
    try {
        const dataAuthorizationCode = await getOne('CredentialsRequest', req.body.issuer_state);
        const newDataAuthorizationCode = {
            credentials_information: dataAuthorizationCode.credentials_information,
            issuer_state: req.body.issuer_state,
            redirect_uri: req.body.redirect_uri,
            request_uri: `urn:ietf:params:oauth:request_uri:${uuidv4()}`,
            code_challenge: req.body.code_challenge,
            code_challenge_methode: req.body.code_challenge_method,
            state: req.body.state
        }

        // Save object in database
        await put('CredentialsRequest', { id: newDataAuthorizationCode.request_uri, object: newDataAuthorizationCode });
        return {
            expires_in: 1000,
            request_uri: newDataAuthorizationCode.request_uri
        };
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: 'Error in PAR', error_description: error.message, status_code: 500 }));
    }
}

async function authorize(req) {
    try {
        if (req.query.request_uri && req.query.request_uri !== '') {
            // Workflow with PAR request
            const redirectUrl = `${process.env.PORTAL_URL}/authorize?request_uri=${req.query.request_uri}`;
            logger.info("Redirect url for portal authorize", { redirectUrl: redirectUrl });
            return { url: redirectUrl };
        }
        else if (req.query.issuer_state && req.query.issuer_state !== '') {
            // Workflow without PAR request
            const dataAuthorizationCode = await getOne('CredentialsRequest', req.query.issuer_state);
            dataAuthorizationCode.redirect_uri = req.query.redirect_uri;
            dataAuthorizationCode.state = req.query.state;
            dataAuthorizationCode.code_challenge = req.query.code_challenge,
                dataAuthorizationCode.code_challenge_methode = req.query.code_challenge_method,
                await put('CredentialsRequest', { id: req.query.issuer_state, object: dataAuthorizationCode });
            const redirectUrl = `${process.env.PORTAL_URL}/authorize?issuer_state=${req.query.issuer_state}`;
            return { url: redirectUrl };
        }
        else throw new HttpError({ message: 'Invalid_request', error_description: 'Authorize format error', status_code: 401 });
    } catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: 'Error in authorize', error_description: error.message, status_code: 500 }));
    }
}


/**
 * 
 */
async function token(req) {
    try {
        // Check grant_type
        let code;
        switch (req.body.grant_type) {
            case 'authorization_code': {
                if (!req.body.code) {
                    throw new HttpError({ message: 'Invalid_request', error_description: 'The code field is missing', status_code: 400 });
                }
                code = req.body.code;
                break;
            }
            case 'urn:ietf:params:oauth:grant-type:pre-authorized_code': {
                if (!req.body["pre-authorized_code"]) {
                    throw new HttpError({ message: 'Invalid_request', error_description: 'The pre-authorized_code field is missing', status_code: 400 });
                }
                code = req.body["pre-authorized_code"];
                break;
            }
            default:
                throw new HttpError({ message: 'Invalid_request', error_description: 'Request format is incorrect, grant is missing', status_code: 400 });
        }

        // Check and get client authentication method
        let clientAuthentificationMethod;
        if (req.body.assertion || req.body.client_assertion)
            clientAuthentificationMethod = 'client_secret_jwt';
        else if (req.headers.authorization)
            clientAuthentificationMethod = 'client_secret_basic';
        else if (req.body.client_id && req.body.client_secret)
            clientAuthentificationMethod = 'client_secret_post';
        else if (req.body.client_id)
            clientAuthentificationMethod = 'client_id';
        else
            clientAuthentificationMethod = null;
        logger.info(`Client authentication method: ${clientAuthentificationMethod}`);

        // Check content of client assertion and proof of possession (PoP)
        if (clientAuthentificationMethod === 'client_secret_jwt') {
            const clientAssertionParts = req.body.client_assertion.split("~");
            const clientAssertion = clientAssertionParts[0];
            const clientId = req.body.client_id;
            if (clientId !== decodeJwt(clientAssertion).sub) {
                throw new HttpError({ message: 'Invalid_request', error_description: 'Client_id does not match client assertion subject', status_code: 401 });
            }

            const poP = clientAssertionParts[1];
            if (decodeJwt(clientAssertion).sub !== decodeJwt(poP).iss) {
                throw new HttpError({ message: 'Invalid_request', error_description: 'sub of client assertion does not match proof of possession iss', status_code: 401 });
            }
        }

        // Get object from database
        const dataCredentialsRequest = await getOne('CredentialsRequest', code);
        if (dataCredentialsRequest == null)
            throw new HttpError({ message: 'Access_denied', error_description: 'Grant code is incorrect', status_code: 400 });

        // Check code verifier
        if (req.body.grant_type == 'authorization_code') {
            const verifyCodeChallenge = (await import('pkce-challenge')).verifyChallenge;
            if (!(await verifyCodeChallenge(req.body.code_verifier, dataCredentialsRequest.code_challenge)))
                throw new HttpError({ message: 'Access_denied', error_description: 'Code verifier is incorrect', status_code: 400 });
            logger.info(`Code verifier is correct`);
        }

        // Check tx_code
        if (dataCredentialsRequest.tx_code_required && !req.body.tx_code)
            throw new HttpError({ message: 'Invalid_request', error_description: 'tx_code is missing', status_code: 400 });
        if (dataCredentialsRequest.tx_code_required && dataCredentialsRequest.tx_code !== req.body.tx_code)
            throw new HttpError({ message: 'Access_denied', error_description: 'tx_code is incorrect', status_code: 400 });

        // Create Access Token
        const access_token = uuidv4();
        const c_nonce = uuidv4();
        const dataCredentialRequest = {
            credentials_information: dataCredentialsRequest.credentials_information,
            access_token: access_token,
            c_nonce: c_nonce
        }

        // Save object in database
        await put('CredentialsRequest', { id: access_token, object: dataCredentialRequest });
        await deleteOne('CredentialsRequest', code);
        const result = {
            access_token: access_token,
            token_type: 'Bearer',
            expires_in: 10000,
            c_nonce: c_nonce,
            c_nonce_expires_in: 86400,
            refresh_token: uuidv4()
        }
        logger.info("Response of Token endpoint", { "response": result });
        return result;
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: 'Error in token', error_description: error.message, status_code: 500 }));
    }
}



async function credential(req) {
    try {
        // Check access_token
        if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) {
            throw new HttpError({ message: 'Invalid_request', error_description: 'The authorization header is missing or invalid', status_code: 400 });
        }
        const access_token = req.headers.authorization.split(' ')[1];
        const dataCredentialRequest = await getOne('CredentialsRequest', access_token);
        if (!dataCredentialRequest.credentials_information && !dataCredentialRequest.c_nonce) {
            throw new HttpError({ message: 'Access_denied', error_description: 'The access_token is invalid', status_code: 401 });
        }

        // Check Request Body - TODO: Check proof (with verifier token) and nonce
        if (req.body.format !== 'ldp_vc' && req.body.format !== 'jwt_vc' && req.body.format !== 'vc+sd-jwt') {
            throw new HttpError({ message: 'Invalid_request', error_description: 'The format is invalid', status_code: 400 });
        }

        // Get the holder public key
        let holderPublicKey;
        if (req.body.proof.proof_type === 'jwt') {
            const decodedJwt = decodeJwt(req.body.proof.jwt);
            if (decodedJwt.hasOwnProperty('iss') && decodedJwt.iss && decodedJwt.iss.startsWith('did:key:')) {
                const keyDidResolver = (await import('key-did-resolver')).getResolver();
                const didResolver = new Resolver(keyDidResolver);
                const doc = await didResolver.resolve(decodedJwt.iss);
                holderPublicKey = doc.didDocument.verificationMethod[0].publicKeyJwk;
            }
            else if (decodedJwt.hasOwnProperty('iss') && decodedJwt.iss && decodedJwt.iss.startsWith('did:jwk')) {
                holderPublicKey = JSON.parse(Buffer.from(decodedJwt.iss.split(':')[2], 'base64url').toString('utf-8'));
            }
            if (!holderPublicKey) {
                const headerProof = decodeProtectedHeader(req.body.proof.jwt);
                if (headerProof.hasOwnProperty('jwk') && headerProof.jwk) holderPublicKey = headerProof.jwk;
            }
            logger.info("holderPublicKey", { "holderPublicKey": holderPublicKey ? holderPublicKey : 'Error to get holderPublicKey' });
        }
        else if (req.body.proof.proof_type === 'ldp_vp') {
            // TODO: For LDP_VP
        }

        // Get the credential to issue if there are more than one
        let credentialToIssue;
        if (req.body.format === 'vc+sd-jwt') {
            const index = dataCredentialRequest.credentials_information.findIndex(item => item.format === req.body.format && item.credential.vct === req.body.vct);
            if (index !== -1) [credentialToIssue] = dataCredentialRequest.credentials_information.splice(index, 1);
        }
        else if (req.body.format === 'ldp_vc') {
            const index = dataCredentialRequest.credentials_information.findIndex(item => item.format === req.body.format && req.body.credential_definition.type.includes(item.type));
            if (index !== -1) [credentialToIssue] = dataCredentialRequest.credentials_information.splice(index, 1);
        }
        if (!credentialToIssue) {
            throw new HttpError({ message: 'Invalid_request', error_description: 'Error to get credential to issue, please verify format and vct', status_code: 400 });
        }
        await put('CredentialsRequest', { id: access_token, object: dataCredentialRequest });

        // Create and send transaction_id if deferred credential
        const c_nonce = uuidv4();
        if (credentialToIssue.deferred) {
            const transactionId = uuidv4();
            await put('DeferredCredentials', {
                id: transactionId, object: {
                    access_token: access_token,
                    transaction_date: Date.now(),
                    holderPublicKey: holderPublicKey,
                    ...credentialToIssue
                }
            });
            return {
                transaction_id: transactionId,
                c_nonce: c_nonce,
                c_nonce_expires_in: "86400"
            }
        }

        // Sign Credential
        const credentialSigned = await signCredential({
            credential: credentialToIssue.credential,
            credentialType: credentialToIssue.type,
            issuerDid: getDidWeb(process.env.BASE_URL),
            issuerUrl: `${process.env.BASE_URL}/${req.params.gateway}/issuer/${req.params.oid4vci_version}`,
            format: credentialToIssue.format,
            cNonce: c_nonce,
            holderPublicKey: holderPublicKey,
            issuerVerificationMethode: `${getDidWeb(process.env.BASE_URL)}#key1`,
        });

        // Save object in database
        await saveCredential(credentialSigned);

        // Delete CredentialsRequest if all credentials are issued
        if (dataCredentialRequest.credentials_information.length === 0)
            await deleteOne('CredentialsRequest', access_token);

        logger.info('Credential Issued', { credential: credentialSigned, c_nonce: c_nonce });
        return {
            format: credentialToIssue.format,
            credential: credentialSigned,
            c_nonce: c_nonce
        };
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
    }
}


async function deferred(req) {
    try {
        if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer '))
            throw new HttpError({ message: 'Invalid_request', error_description: 'Invalid_transaction_id', status_code: 400 });

        const dataCredentialRequest = await getOne('DeferredCredentials', req.body.transaction_id);
        if (!dataCredentialRequest.hasOwnProperty('credential') || !dataCredentialRequest.hasOwnProperty('access_token') || dataCredentialRequest.access_token !== req.headers.authorization.split(' ')[1])
            throw new HttpError({ message: 'Invalid_request', error_description: 'Invalid_transaction_id', status_code: 400 });

        if (Date.now() < (dataCredentialRequest.transaction_date + dataCredentialRequest.deferred_time))
            throw new HttpError({ message: 'Invalid_request', error_description: 'Issuance_pending', status_code: 400 });

        // Sign Credential
        const credentialSigned = await signCredential({
            credential: dataCredentialRequest.credential,
            credentialType: dataCredentialRequest.type,
            issuerDid: getDidWeb(process.env.BASE_URL),
            issuerUrl: `${process.env.BASE_URL}/${req.params.gateway}/issuer/${req.params.oid4vci_version}`,
            format: dataCredentialRequest.format,
            cNonce: dataCredentialRequest.c_nonce,
            holderPublicKey: dataCredentialRequest.holderPublicKey,
            issuerVerificationMethode: `${getDidWeb(process.env.BASE_URL)}#key1`,
        });

        await saveCredential(credentialSigned);
        await deleteOne('DeferredCredentials', req.body.transaction_id);
        logger.info("Credential Deferred Issued", { transaction_id: req.body.transaction_id, credential: credentialSigned, c_nonce: dataCredentialRequest.c_nonce });
        return {
            format: dataCredentialRequest.format,
            credential: credentialSigned,
            c_nonce: dataCredentialRequest.c_nonce,
            c_nonce_expires_in: 86400
        };
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: 'Error in deferred', error_description: error.message, status_code: 500 }));
    }
}


module.exports = {
    par,
    jwks,
    token,
    deferred,
    authorize,
    credential,
    getOpenidConfiguration,
    createNewAuthorizationCode,
    getOauthAuthorizationServer,
    createNewPreAuthorizedWorkflow
}