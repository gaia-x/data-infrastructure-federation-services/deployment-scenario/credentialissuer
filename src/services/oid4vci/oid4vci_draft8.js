const axios = require('axios');
const utils = require("@utils/utils");
const { v4: uuidv4 } = require('uuid');
const logger = require('@services/logger');
const HttpError = require('@utils/httpError');
const { getDidWeb } = require('@services/didDocument');
const routingTable = require('@services/routingTableManager');
const { put, getOne, deleteOne } = require('@services/mongodb');
const { saveCredential, signCredential } = require('@services/credential');



/**
 * Create a new Pre Authorized Workflow
 * @param {*} body Object with issuance informations
 *      @example body = {
 *                  gateway: String - Gateway name
 *                  issuance_options: Object - Issuance options,
 *                      @example issuance_options = {
 *                          oid4vci_version: String - version of OID4VCI
 *                          oidc_prefix: String - prefix for the Openid URI
 *                      }
 *                  credentials_information: Array - Credentials information
 *                      @example issuance_options = [{
 *                          format: String - The format of the credential
 *                          type: String - The type of the credential
 *                          credential: Object - The credential
 *                      }, ...]
 *               }
 * @returns The Openid URI
 */
async function createNewPreAuthorizedWorkflow({ gateway, issuance_options, credentials_information }) {
    try {
        // Create object for pre authorized workflow
        let dataCredentialPreAuthorization = {
            'credentials_information': credentials_information,
            "pre-authorized_code": uuidv4(),
            "nonce": uuidv4()
        }

        // Save object in database
        await put('CredentialsRequest', { id: dataCredentialPreAuthorization["pre-authorized_code"], object: dataCredentialPreAuthorization });

        // Create Openid URI
        const oidcPrefix = issuance_options.oidc_prefix || 'openid://?';
        const issuer = encodeURIComponent(`${process.env.BASE_URL}/${gateway}/issuer/oid4vci_draft8`);
        const oidcUri = {
            issuer: `issuer=${issuer}`,
            credential_type: `credential_type=${credentials_information.map(item => item.type)}`,
            pre_authorized_code: `pre-authorized_code=${dataCredentialPreAuthorization["pre-authorized_code"]}`,
            user_pin_required: `user_pin_required=false`,
        }
        const encodedOidcUri = `${oidcPrefix}${Object.values(oidcUri).join("&")}`;

        logger.info('Openid Uri created', { "openid-uri": encodedOidcUri, "nonce": dataCredentialPreAuthorization.nonce });
        return encodedOidcUri;
    }
    catch (error) {
        throw new HttpError(`Error in createNewPreAuthorizedWorkflow (darft8): ${error.message}`, 500);
    }
}


async function getOpenidConfiguration(req) {
    try {
        const oidcConfiguration = await getOne('Configuration', 'openid-configuration');
        const gatewayUrl = routingTable.getConnectionUrl(req.params.gateway, 'credentials-supported');
        const response = await axios.get(gatewayUrl);
        const valuesToReplace = {
            BASE_URL: process.env.BASE_URL,
            GATEWAY_NAME: req.params.gateway,
            OID4VCI_VERSION: req.params.oid4vci_version
        };

        const replacedJson = utils.replaceValuesInJSON(oidcConfiguration, valuesToReplace);
        replacedJson.credentials_supported = response.data.credentials_supported;
        return replacedJson;
    } catch (error) {
        throw (error instanceof HttpError ? error : new HttpError(error.message, 500));
    }
};

/**
 * 
 */
async function token(req) {
    try {
        if (!req.body.grant_type || req.body.grant_type !== 'urn:ietf:params:oauth:grant-type:pre-authorized_code') {
            throw new HttpError(`The grant_type field is missing or not supported : ${req.body.grant_type}`, 400);
        }

        if (!req.body["pre-authorized_code"]) {
            throw new HttpError(`The pre-authorized_code field is missing : ${req.body["pre-authorized_code"]}`, 400);
        }

        let dataCredentialPreAuthorization = await getOne('CredentialsRequest', req.body["pre-authorized_code"]);
        if (!dataCredentialPreAuthorization.credentials_information) {
            throw new HttpError('The pre-authorized_code is invalid', 401);
        }

        // Create Access Token
        const access_token = uuidv4();
        const c_nonce = uuidv4();
        const dataCredentialRequest = {
            credentials_information: dataCredentialPreAuthorization.credentials_information,
            access_token: access_token,
            c_nonce: c_nonce
        }

        // Save object in database
        await put('CredentialsRequest', { id: access_token, object: dataCredentialRequest });
        await deleteOne('CredentialsRequest', req.body["pre-authorized_code"]);

        logger.info("Access Token created", { "access_token": access_token, "c_nonce": c_nonce });
        return {
            access_token: access_token,
            token_type: 'Bearer',
            expires_in: 86400,
            c_nonce: c_nonce,
            c_nonce_expires_in: 86400
        };
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError(error.message, 500));
    }
}



async function credential(req) {
    try {
        // Check access_token
        if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) {
            throw new HttpError('The authorization header is missing or invalid', 401);
        }
        const access_token = req.headers.authorization.split(' ')[1];
        const dataCredentialRequest = await getOne('CredentialsRequest', access_token);
        if (!dataCredentialRequest.credentials_information && !dataCredentialRequest.c_nonce) {
            throw new HttpError('The access_token is invalid', 401);
        }

        // Check Request Body
        if (!req.body.format || !req.body.type || !req.body.proof) {
            throw new HttpError('The request body is invalid', 400);
        }
        if (req.body.proof.proof_type !== 'jwt') {
            throw new HttpError('The proof_type is invalid', 400);
        }
        if (req.body.format !== 'ldp_vc' && req.body.format !== 'jwt_vc') {
            throw new HttpError('The format is invalid', 400);
        }

        // Sign Credential
        const c_nonce = uuidv4();
        const credentialSigned = await signCredential({
            credential: dataCredentialRequest.credentials_information[0].credential,
            credentialType: dataCredentialRequest.credentials_information[0].type,
            issuerDid: getDidWeb(process.env.BASE_URL),
            issuerUrl: `${process.env.BASE_URL}/${req.params.gateway}/issuer/${req.params.oid4vci_version}`,
            format: dataCredentialRequest.credentials_information[0].format,
            cNonce: c_nonce,
            issuerVerificationMethode: `${getDidWeb(process.env.BASE_URL)}#key1`,
        });

        // Save object in database
        await saveCredential(credentialSigned);
        await deleteOne('CredentialsRequest', access_token);

        logger.info('Credential Issued', { credential: credentialSigned, c_nonce: c_nonce });
        return {
            format: dataCredentialRequest.credentials_information[0].format,
            credential: credentialSigned,
            c_nonce: c_nonce,
            c_nonce_expires_in: 86400
        };
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError(error.message, 500));
    }
}


module.exports = {
    token,
    credential,
    getOpenidConfiguration,
    createNewPreAuthorizedWorkflow
}