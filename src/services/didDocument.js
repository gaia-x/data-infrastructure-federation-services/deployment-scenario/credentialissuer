const { exportJWK } = require('jose');
const { put } = require('./mongodb.js');
const { createPublicKey } = require('crypto');
const HttpError = require("@utils/httpError");


/**
 * Return the did:web of the issuer by the BASE_URL
 * 
 * @returns {string} did:web
 */
function getDidWeb() {
    return `did:web:${process.env.BASE_URL.replace(/http[s]?:\/\//, '')
        .replace(':', '%3A')    // encode port ':' as '%3A' in did:web
        .replace(/\//g, ':')}`
}

/**
 * Generates a DID document using the provided private key and Base URL.
 *
 * @return {Promise<Object>} The generated DID document.
 */
async function generateDidDocument() {
    try {
        const keyObject = createPublicKey({ key: JSON.parse(process.env.PRIVATE_KEY), format: 'jwk' });
        const publicKeyJwk = await exportJWK(keyObject);

        if (publicKeyJwk.kty === 'RSA') {
            publicKeyJwk.alg = JSON.parse(process.env.PRIVATE_KEY).alg || 'PS256';
        }

        const didWeb = getDidWeb();
        const identifier = `${didWeb}#key1`;

        return {
            '@context': [
                'https://www.w3.org/ns/did/v1',
                'https://w3id.org/security/suites/jws-2020/v1'
            ],
            id: didWeb,
            verificationMethod: [
                {
                    '@context': 'https://w3c-ccg.github.io/lds-jws2020/contexts/v1/',
                    id: identifier,
                    type: 'JsonWebKey2020',
                    controller: didWeb,
                    publicKeyJwk
                }
            ],
            assertionMethod: [identifier]
        };
    }
    catch (error) {
        throw new HttpError({ message: 'Error generating DID document', error_description: error.message, status_code: 500 });
    }
}




/**
 * Initializes the JWKs (JSON Web Key) for the application by the env var PRIVATE_KEY.
 *
 * @return {Promise<void>} A promise that resolves once the JWKs are initialized.
 */
async function initJWKs() {
    try {
        const privateJwk = JSON.parse(process.env.PRIVATE_KEY);
        const keyObject = createPublicKey({ key: privateJwk, format: 'jwk' });
        const publicKeyJwk = await exportJWK(keyObject);

        if (publicKeyJwk.kty === 'RSA') {
            publicKeyJwk.alg = privateJwk.alg || 'PS256';
        }

        const kid = privateJwk.kid;
        if (kid !== undefined && kid !== '') publicKeyJwk.kid = kid;
        else {
            console.log('Error, kid not specified in private JWK');
            process.exit(1);
        }

        await put('JWKs', { id: publicKeyJwk.kid, object: publicKeyJwk });
    }
    catch (error) {
        console.log(`Error to store public JWK in MongoDB (Check that the PRIVATE_KEY env var is JSON valid and contains a kid property): ${error.message}`);
        process.exit(1);
    }
}



module.exports = {
    initJWKs,
    getDidWeb,
    generateDidDocument,
}