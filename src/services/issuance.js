const axios = require('axios');
const HttpError = require("@utils/httpError");
const routingTable = require('@services/routingTableManager');


async function createNewIssuance({ gateway, issuance_options, credentials_information }) {
    const oid4vciModule = await import(`./oid4vci/${issuance_options.oid4vci_version}.js`);
    if (issuance_options.mode === 'pre-authorized_code')
        return await oid4vciModule["createNewPreAuthorizedWorkflow"]({ gateway, issuance_options, credentials_information });
    else if (issuance_options.mode === 'authorization_code')
        return await oid4vciModule["createNewAuthorizationCode"]({ gateway, issuance_options, credentials_information });    
    else throw new HttpError({ message: 'Invalid_request', error_description: `Mode not supported for new issuance: ${req.body.issuance_options.mode}`, status_code: 400 });
}


async function getCredentialInformations(gateway, vcType, data) {
    try {
        const gatewayUrl = `${routingTable.getConnectionUrl(gateway, 'issuance')}/${vcType}`;
        let response = await axios.post(gatewayUrl, data);
        return response.data.credentials_information;
    }
    catch (error) {
        if (error.isAxiosError && error.response && error.response.status != 500) {
            throw new HttpError({ message: error.response.data.error, status_code: error.response.status });
        }
        throw(error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
    }
}




module.exports = {
    createNewIssuance,
    getCredentialInformations
}