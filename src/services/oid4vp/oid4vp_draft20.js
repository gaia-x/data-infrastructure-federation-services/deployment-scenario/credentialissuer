const { v4: uuidv4 } = require('uuid');
const logger = require('@services/logger');
const HttpError = require('@utils/httpError');
const { getDidWeb } = require('@services/didDocument');
const { put, getOne, deleteOne } = require('@services/mongodb');
const { verifyVerifiablePresentation } = require('@services/credential');
const { signPayloadJWT, generateX509selfSignedCertificate } = require('@services/signature');


async function createNewPresentation({ gateway, presentation_options, issuance_options, presentation_definition }) {
    try {
        const state = uuidv4();
        const nonce = uuidv4();
        const clientMetadata = await getOne('Configuration', 'client-metadata');
        const callbackUri = `${process.env.BASE_URL}/${gateway}/verifier/${presentation_options.oid4vp_version}`;

        // Payload of presentation request object
        const payloadRequestUri = {
            aud: 'https://self-issued.me/v2',
            client_id: process.env.BASE_URL,
            client_id_scheme: "x509_san_dns",
            client_metadata: clientMetadata,
            iat: Math.floor(Date.now() / 1000),
            exp: Math.floor((Date.now() + 3600000) / 1000),   // 3600000 = 1 hour
            iss: getDidWeb(process.env.BASE_URL),
            nonce: nonce,
            presentation_definition: presentation_definition,
            response_mode: 'direct_post',
            response_type: 'vp_token',
            response_uri: `${callbackUri}/${state}`,
            state: state
        }

        // Header of presentation request object
        const selfSignedCertificate = await generateX509selfSignedCertificate(JSON.parse(process.env.PRIVATE_KEY), process.env.BASE_URL);
        const header = {
            "typ": "oauth-authz-req+jwt",
            "x5c": [selfSignedCertificate]
        }

        // TODO: add support for multiple client_id_scheme, example for kid:
        // const header = {
        //     "typ": "oauth-authz-req+jwt",
        //     "kid": `${getDidWeb(process.env.BASE_URL)}#key-1`
        // }

        const presentationToken = await signPayloadJWT(JSON.parse(process.env.PRIVATE_KEY), header, payloadRequestUri);
        const objectToSave = {
            gateway: gateway,
            presentation_token: presentationToken,
            response_mode: 'direct_post.jwt',       // TODO: add support for multiple response_mode
            nonce: nonce,
            status: 'Pending'
        }

        if (presentation_options.gateway_service && presentation_options.gateway_service !== '') {
            objectToSave.gateway_service = presentation_options.gateway_service;
            objectToSave.issuance_options = issuance_options;
        }

        await put('PresentationRequest', { id: state, object: objectToSave });

        let presentationUri = presentation_options.oidc_prefix || 'openid-vc://?';
        presentationUri += `client_id=${process.env.BASE_URL}&`;
        presentationUri += `request_uri=${callbackUri}/request_uri/${state}`;
        
        logger.info('New Presentation created', { openidPresentationUri: presentationUri });
        return { openidUri: presentationUri, state: state };
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: 'Error to create new presentation oid4vp draft20', error_description: error.message, status_code: 500 }));
    }
}


async function getRequestUriResult(req) {
    try {
        const vpRequest = await getOne("PresentationRequest", req.params.state);
        return { type: 'oauth-authz-req+jwt', data: vpRequest.presentation_token };
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
    }
}


async function callbackPresentation(req) {
    try {
        // const vp = JSON.parse(decodeURIComponent(req.body.vp_token));
        // if (! await verifyVerifiablePresentation(vp)) {
        //     deleteOne("PresentationRequest", req.params.state);
        //     throw new HttpError("Invalid Verifiable Presentation", 400);
        // }

        let vpRequest = await getOne('PresentationRequest', req.params.state);
        if (vpRequest.status && vpRequest.status === 'Pending') {
            let vp;
            if (req.body.vp_token && req.body.vp_token !== '')
                vp = req.body.vp_token
            else if (req.body.response && req.body.response !== '')
                vp = req.body.response
            else
                throw new HttpError({ message: 'Invalid_request', error_description: 'No vp_token or response in body', status_code: 400 });

            vpRequest.vp = vp;
            vpRequest.status = 'Received';
            logger.info(`Verifiable Presentation received for state ${req.params.state}}`, { vp: vp });
            await put('PresentationRequest', { id: req.params.state, object: vpRequest });
            return { type: 'json', data: { message: `Presentation received for state ${req.params.state}` } };
        } else {
            throw new HttpError({ message: 'Invalid_request', error_description: `Presentation already received for state ${req.params.state}`, status_code: 400 });
        }
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
    }
}


module.exports = {
    createNewPresentation,
    getRequestUriResult,
    callbackPresentation
}