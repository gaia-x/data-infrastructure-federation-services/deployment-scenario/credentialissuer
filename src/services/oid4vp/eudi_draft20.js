const { v4: uuidv4 } = require('uuid');
const logger = require('@services/logger');
const HttpError = require('@utils/httpError');
const { getDidWeb } = require('@services/didDocument');
const { put, getOne, deleteOne } = require('@services/mongodb');
const { verifyVerifiablePresentation } = require('@services/credential');
const { signPayloadJWT, generateX509selfSignedCertificate } = require('@services/signature');



async function createNewPresentation({ gateway, presentation_options, issuance_options, presentation_definition }) {
    try {
        const state = uuidv4();
        const nonce = uuidv4();
        const clientMetadata = await getOne('Configuration', 'client-metadata');
        const callbackUri = `${process.env.BASE_URL}/${gateway}/verifier/${presentation_options.oid4vp_version}`;

        // Payload of presentation request object
        const payloadRequestUri = {
            response_uri: `${callbackUri}/${state}`,
            client_id_scheme: "x509_san_dns",
            response_type: "vp_token",
            nonce: nonce,
            client_id: process.env.BASE_URL.replace(/http[s]?:\/\//, ''),
            response_mode: "direct_post.jwt",
            aud: "https://self-issued.me/v2",
            scope: "",
            presentation_definition: {
                "id": "32f54163-7166-48f1-93d8-ff217bdb0653",
                "input_descriptors": [
                    {
                        "id": "eu.europa.ec.eudiw.pid.1",
                        "name": "EUDI PID",
                        "purpose": "We need to verify your identity",
                        "format": {
                            "mso_mdoc": {
                                "alg": [
                                    "ES256",
                                    "ES384",
                                    "ES512",
                                    "EdDSA",
                                    "ESB256",
                                    "ESB320",
                                    "ESB384",
                                    "ESB512"
                                ]
                            }
                        },
                        "constraints": {
                            "fields": [
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['family_name']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['given_name']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['birth_date']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['age_over_18']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['age_in_years']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['age_birth_year']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['family_name_birth']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['given_name_birth']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['birth_place']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['birth_country']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['birth_state']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['birth_city']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['resident_address']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['resident_country']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['resident_state']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['resident_city']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['resident_postal_code']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['resident_street']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['resident_house_number']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['gender']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['nationality']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['issuance_date']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['expiry_date']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['issuing_authority']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['document_number']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['administrative_number']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['issuing_country']"
                                    ],
                                    "intent_to_retain": false
                                },
                                {
                                    "path": [
                                        "$['eu.europa.ec.eudiw.pid.1']['issuing_jurisdiction']"
                                    ],
                                    "intent_to_retain": false
                                }
                            ]
                        }
                    }
                ]
            },
            "state": state,
            "iat": Math.floor(Date.now() / 1000),
            "client_metadata": {
                "authorization_encrypted_response_alg": "ECDH-ES",
                "authorization_encrypted_response_enc": "A128CBC-HS256",
                "jwks_uri": `${process.env.BASE_URL}/.well-known/jwks`,
                "subject_syntax_types_supported": [
                    "did:key",
                    "urn:ietf:params:oauth:jwk-thumbprint"
                ]
            }
        }


        // Header of presentation request object
        const selfSignedCertificate = await generateX509selfSignedCertificate(JSON.parse(process.env.PRIVATE_KEY), process.env.BASE_URL.replace(/http[s]?:\/\//, ''));
        const header = {
            "typ": "oauth-authz-req+jwt",
            "x5c": [selfSignedCertificate]
        }

        const presentationToken = await signPayloadJWT(JSON.parse(process.env.PRIVATE_KEY), header, payloadRequestUri);
        const objectToSave = {
            gateway: gateway,
            presentation_token: presentationToken,
            response_mode: 'direct_post.jwt',       // TODO: add support for multiple response_mode
            nonce: nonce,
            status: 'Pending'
        }

        if (presentation_options.gateway_service && presentation_options.gateway_service !== '') {
            objectToSave.gateway_service = presentation_options.gateway_service;
            objectToSave.issuance_options = issuance_options;
        }

        await put('PresentationRequest', { id: state, object: objectToSave });

        let presentationUri = `eudi-openid4vp://${process.env.BASE_URL.replace(/http[s]?:\/\//, '')}?`;
        presentationUri += `client_id=${process.env.BASE_URL.replace(/http[s]?:\/\//, '')}&`;
        presentationUri += `request_uri=${callbackUri}/request_uri/${state}`;

        logger.info('New Presentation created', { openidPresentationUri: presentationUri });
        return { openidUri: presentationUri, state: state };
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: 'Error to create new presentation oid4vp draft20', error_description: error.message, status_code: 500 }));
    }
}

async function getRequestUriResult(req) {
    try {
        const vpRequest = await getOne("PresentationRequest", req.params.state);
        return { type: 'oauth-authz-req+jwt', data: vpRequest.presentation_token };
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
    }
}


async function callbackPresentation(req) {
    try {
        // const vp = JSON.parse(decodeURIComponent(req.body.vp_token));
        // if (! await verifyVerifiablePresentation(vp)) {
        //     deleteOne("PresentationRequest", req.params.state);
        //     throw new HttpError("Invalid Verifiable Presentation", 400);
        // }

        let vpRequest = await getOne('PresentationRequest', req.params.state);
        if (vpRequest.status && vpRequest.status === 'Pending') {
            let vp;
            if (req.body.vp_token && req.body.vp_token !== '')
                vp = req.body.vp_token
            else if (req.body.response && req.body.response !== '')
                vp = req.body.response
            else
                throw new HttpError({ message: 'Invalid_request', error_description: 'No vp_token or response in body', status_code: 400 });

            vpRequest.vp = vp;
            vpRequest.status = 'Received';
            logger.info(`Verifiable Presentation received for state ${req.params.state}}`, { vp: vp });
            await put('PresentationRequest', { id: req.params.state, object: vpRequest });
            return { type: 'json', data: { message: `Presentation received for state ${req.params.state}` } };
        } else {
            throw new HttpError({ message: 'Invalid_request', error_description: `Presentation already received for state ${req.params.state}`, status_code: 400 });
        }
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
    }
}


module.exports = {
    createNewPresentation,
    getRequestUriResult,
    callbackPresentation
}