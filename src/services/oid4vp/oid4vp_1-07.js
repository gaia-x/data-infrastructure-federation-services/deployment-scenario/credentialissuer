const { v4: uuidv4 } = require('uuid');
const logger = require('@services/logger');
const HttpError = require('@utils/httpError');
const { put, getOne, deleteOne } = require('@services/mongodb');
const { verifyVerifiablePresentation } = require('@services/credential');


async function createNewPresentation({ gateway, presentation_options, issuance_options, presentation_definition }) {
    const state = uuidv4();
    const nonce = uuidv4();
    const callbackUri = `${process.env.BASE_URL}/${gateway}/verifier/${presentation_options.oid4vp_version}`;
    const objectToSave = {
        gateway: gateway,
        nonce: nonce,
        status: 'Pending'
    }

    if (presentation_options.gateway_service && presentation_options.gateway_service !== '') {
        objectToSave.gateway_service = presentation_options.gateway_service;
        objectToSave.issuance_options = issuance_options;
    }
    await put('PresentationRequest', { id: state, object: objectToSave });

    let request = presentation_options.oidc_prefix || 'openid://?';
    request += 'scope=openid&';
    request += 'presentation_definition=' + encodeURIComponent(JSON.stringify(presentation_definition)) + "&";
    request += 'response_type=vp_token&';
    request += 'redirect_uri=' + encodeURIComponent(`${callbackUri}/${state}`) +  "&";
    request += 'state=' + state + '&';
    request += 'nonce=' + nonce + '&';
    request += 'client_id=' + encodeURIComponent(`${callbackUri}/${state}`) + "&";
    request += 'response_mode=post';

    logger.info("New Presentation created", { openidPresentationUri: request });
    return { openidUri: request, state: state };
}


async function callbackPresentation(req) {
    try {
        // const vp = JSON.parse(decodeURIComponent(req.body.vp_token));
        // if (! await verifyVerifiablePresentation(vp)) {
        //     deleteOne("PresentationRequest", req.params.state);
        //     throw new HttpError("Invalid Verifiable Presentation", 400);
        // }

        let vpRequest = await getOne('PresentationRequest', req.params.state);
        if (vpRequest.status && vpRequest.status === 'Pending') {
            vpRequest.status = 'Received';
            vpRequest.vp = req.body.vp_token;
            logger.info(`Verifiable Presentation received for state ${req.params.state}}`, { vp: req.body.vp_token });
            await put('PresentationRequest', { id: req.params.state, object: vpRequest });
            return { type: 'json', data: { message: `Presentation received for state ${req.params.state}` }};
        } else {
            throw new HttpError({ message: 'Invalid_request', error_description: `Presentation already received for state ${req.params.state}`, status_code: 400 });
        }
    }
    catch (error) {
        throw(error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
    }
}


module.exports = {
    callbackPresentation,
    createNewPresentation
}