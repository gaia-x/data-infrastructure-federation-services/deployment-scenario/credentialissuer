const logger = require('./logger');
const x509 = require("@peculiar/x509");
const HttpError = require("@utils/httpError");
const { Issuer } = require('@meeco/sd-jwt-vc');
const { base64encode } = require('@meeco/sd-jwt');
const { Crypto } = require("@peculiar/webcrypto");
const { SignJWT, importJWK, exportJWK } = require('jose');
const { createHash, createPublicKey, randomBytes } = require('crypto');
const { GaiaXSignatureSigner } = require('@gaia-x/json-web-signature-2020');

const crypto = new Crypto();
x509.cryptoProvider.set(crypto);


/**
 * Finds the algorithm based on the key type and curve parameters.
 *
 * @param {string} kty - The key type parameter
 * @param {string} crv - The curve parameter
 * @return {string} The algorithm based on the parameters
 */
function findAlgFromKeyAttributes(kty, crv) {
    if (kty === 'RSA') {
        return 'RS256';
    }

    switch (crv) {
        case 'Ed25519':
            return 'EdDSA';
        case 'P-256':
            return 'ES256';
        case 'P-384':
            return 'ES384';
        case 'P-521':
            return 'ES512';
        case 'secp256k1':
            return 'ES256K';
        default:
            throw new HttpError({ message: 'Invalid_request', error_description: `Unsupported key configuration: ${kty}, ${crv}`, status_code: 400 });
    }
}


/**
 * Return the algorithm for x509 certificate based on the parameters kty and crv
 *
 * @param {string} kty - The key type parameter
 * @param {string} crv - The curve parameter
 * @return {string} The algorithm based on the parameters
 */
function findAlgForX509(kty, crv) {
    if (kty === 'RSA') {
        return { name: "RSASSA-PKCS1-v1_5", hash: { name: "SHA-256" }}
    }

    switch (crv) {
        case 'Ed25519':
            return { name: "EdDSA", namedCurve: "Ed25519" }
        case 'P-256':
            return { name: "ECDSA", namedCurve: "P-256" }
        case 'P-384':
            return { name: "ECDSA", namedCurve: "P-384" }
        case 'P-521':
            return { name: "ECDSA", namedCurve: "P-521" }
        case 'secp256k1':
            return { name: "ECDSA", namedCurve: "secp256k1" }
        default:
            throw new HttpError({ message: 'Invalid_request', error_description: `Unsupported key configuration: ${kty}, ${crv}`, status_code: 400 });
    }
}


/**
 * 
 * @param {object} privateKeyJwk - The private key JWK used for signing.
 * @param {string} hostname The hostname to insert in the certificate
 * @returns {string} The base64 encoded certificate
 */
async function generateX509selfSignedCertificate(privateKeyJwk, hostname) {
    /* Import and create key object */
    const alg = findAlgForX509(privateKeyJwk.kty, privateKeyJwk.crv);
    const publicKeyObject = createPublicKey({ key: privateKeyJwk, format: 'jwk' });
    const publicKeyJWK = await exportJWK(publicKeyObject);
    const privateKey = await crypto.subtle.importKey('jwk', privateKeyJwk, alg, true, ["sign"]);
    const publicKey = await crypto.subtle.importKey('jwk', publicKeyJWK, alg, true, ["verify"]);
    const keys = { privateKey, publicKey };

    /* Certificate valid for 2 years */
    const currentDate = new Date();
    const nextYear = new Date(currentDate);
    nextYear.setFullYear(currentDate.getFullYear() + 2);

    /* Create self signed certificate */
    const cert = await x509.X509CertificateGenerator.createSelfSigned({
        serialNumber: randomBytes(20).toString('hex'),
        name: `CN=${hostname}, C=FR`,
        notBefore: currentDate,
        notAfter: nextYear,
        signingAlgorithm: alg,
        keys,
        extensions: [
            new x509.BasicConstraintsExtension(true, 2, true),
            new x509.ExtendedKeyUsageExtension(["1.2.3.4.5.6.7", "2.3.4.5.6.7.8"], true),
            new x509.KeyUsagesExtension(x509.KeyUsageFlags.keyCertSign | x509.KeyUsageFlags.cRLSign, true),
            await x509.SubjectKeyIdentifierExtension.create(keys.publicKey),
            new x509.SubjectAlternativeNameExtension([
                new x509.GeneralName("dns", hostname)
            ], true),
        ]
    });

    return cert.toString('base64');
}


/**
 * Asynchronously signs a payload using the provided private key JWK and key ID.
 *
 * @param {object} privateKeyJwk - The private key JWK used for signing.
 * @param {string} header - The header of the JWT.
 * @param {string} payload - The payload to be signed.
 * @return {Promise<string>} A Promise that resolves to the signed JWT.
 */
async function signPayloadJWT(privateKeyJwk, header, payload) {
    try {
        const alg = findAlgFromKeyAttributes(privateKeyJwk.kty, privateKeyJwk.crv);
        const privateKey = await importJWK(privateKeyJwk, alg);
        const jwt = await new SignJWT(payload)
            .setProtectedHeader({ alg: alg, ...header })
            .sign(privateKey);

        logger.info("JWT Signed", { "jwt": jwt });
        return jwt;
    }
    catch (error) {
        throw (error instanceof HttpError ? error : new HttpError({ message: 'Error to sign JWT', error_description: error.message, status_code: 500 }));
    }
}


/**
 * Sign Credential in JSON-LD format
 * @param {*} credentialToSign The credential to sign
 * @param {*} issuerVerificationMethode Issuer verification method
 * @param {*} issuerPrivateKey The private JWK of the issuer
 * @returns Credential signed in JSON-LD format
 */
async function signJsonldCredential(credentialToSign, issuerVerificationMethode, privateKeyJwk) {
    try {
        const alg = findAlgFromKeyAttributes(privateKeyJwk.kty, privateKeyJwk.crv);
        const signer = new GaiaXSignatureSigner({
            privateKey: await importJWK(privateKeyJwk, alg),
            privateKeyAlg: alg,
            verificationMethod: issuerVerificationMethode,
            safe: false
        });

        return await signer.sign(credentialToSign);
    }
    catch (error) {
        throw new HttpError({ message: `Error to sign JSON-LD Credential ${JSON.stringify(credentialToSign)}`, error_description: error.message, status_code: 500 });
    }
}


/**
 * Build and sign a Credential in SD-JWT format
 * @param {*} sdJWTHeader The main header of the SD-JWT (kid actually)
 * @param {*} sdJWTPayload The main payload of the SD-JWT
 * @param {*} vcClaims The credential values ​​in plain text
 *    @example: { "name": "John Doe", "age": 30 }
 * @param {*} sdVCClaimsDisclosureFrame The credential properties which must be in selective disclosure
 *    @example: { "_sd": [ "name", "age" ] }
 * @param {*} privateKeyJwk The private KEY to sign crdential in JWK format
 * @returns Credential signed in SD-JWT format
 */
async function signSdJWTCredential(sdJWTHeader, sdJWTPayload, vcClaims, sdVCClaimsDisclosureFrame, privateKeyJwk) {
    const hasherCallbackFn = function (alg) {
        return (data) => {
            const digest = createHash(alg).update(data).digest();
            return base64encode(digest);
        };
    };

    const signerCallbackFn = function (privateKey) {
        return async (protectedHeader, payload) => {
            return (await new SignJWT(payload).setProtectedHeader(protectedHeader).sign(privateKey)).split('.').pop();
        };
    };

    try {
        const alg = findAlgFromKeyAttributes(privateKeyJwk.kty, privateKeyJwk.crv);
        const hasher = {
            alg: 'sha-256',
            callback: hasherCallbackFn('sha-256'),
        };

        const signer = {
            alg: alg,
            callback: signerCallbackFn(await importJWK(privateKeyJwk, alg)),
        };

        const issuer = new Issuer(signer, hasher);
        return await issuer.createSignedVCSDJWT({ sdJWTHeader, vcClaims, sdJWTPayload, sdVCClaimsDisclosureFrame });
    }
    catch (error) {
        throw new HttpError({ message: 'Error to sign SD-JWT Credential', error_description: error.message, status_code: 500 });
    }
}


module.exports = {
    generateX509selfSignedCertificate,
    signJsonldCredential,
    signSdJWTCredential,
    signPayloadJWT
}