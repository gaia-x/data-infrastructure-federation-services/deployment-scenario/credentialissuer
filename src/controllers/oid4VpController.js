const logger = require('@services/logger');
const HttpError = require('@utils/httpError');

exports.callbackPresentation = (req, res, next) => handleRequest(req, res, next, "callbackPresentation");
exports.getRequestUriResult = (req, res, next) => handleRequest(req, res, next, "getRequestUriResult");

async function handleRequest(req, res, next, action) {
  try {
    if (!req.params.oid4vp_version.match(/^[a-zA-Z0-9-_]+$/)) {
      logger.warn("Potential threat to user input regarding OID4VP version.", { "Requested version": req.params.oid4vp_version });
      throw new HttpError({ message: 'Invalid_request', error_description: 'The version doesn\'t exist', status_code: 404 });
    }

    const oid4vpModule = await import(`../services/oid4vp/${req.params.oid4vp_version}.js`);
    if (oid4vpModule[action]) {
      const result = await oid4vpModule[action](req);
      res.setHeader('Cache-Control', 'no-store');
      if (result.type === 'json') {
        res.status(200).json(result.data);
      }
      else if (result.type === 'oauth-authz-req+jwt') {
        res.setHeader('Content-Type', 'application/oauth-authz-req+jwt');
        res.send(result.data);
      }
      else {
        res.send(result);
      }
    } else {
      throw new HttpError({ message: 'Invalid_request', error_description: 'The version doesn\'t exist', status_code: 404 });
    }
  } catch (error) {
    next(error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
  }
}