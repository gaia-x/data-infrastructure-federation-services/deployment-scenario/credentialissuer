const { getOneCredential, findManyCredential, updateStatusCredential } = require('@services/credential');

  
exports.getCredentials = async (req, res, next) => {
  try {
    /* 
    req.query du type :
    { 
      "filter": { "status": "alive" },
      "sort": { "id": "1" },
      "limit": "3",
      "offset": "1"
    }
    */
    if (req.query.filter) req.query.filter = JSON.parse(req.query.filter);
    if (req.query.sort) req.query.sort = JSON.parse(req.query.sort);

    res.json(await findManyCredential(req.query));
  } catch (error) {
    next(new Error(`Error in getCredentials : ${error}`));
  }
};


exports.getStatus = async (req, res, next) => {
  try {
    let credential = await getOneCredential(req.params.idVC);
    if (credential) res.json({ status: credential.status });
    else res.status(404).json({ message: `No VC with id : ${req.params.idVC}`})
  }
  catch (error) {
    next(new Error(`Error in getStatus : ${error}`));
  }
}


exports.updateStatus = async (req, res, next) => {
  try {
    res.send(await updateStatusCredential(req.params.idVC, req.body.newStatus));
  }
  catch (error) {
    next(new Error(`Error in updateStatus : ${error}`));
  }
};