const axios = require('axios');
const { exportJWK } = require('jose');
const logger = require('@services/logger');
const HttpError = require('@utils/httpError');
const { createPublicKey } = require('crypto');
const issuanceService = require('@services/issuance');
const { validateOidcVersion } = require('@utils/utils');
const presentationService = require('@services/presentation');
const routingTable = require('@services/routingTableManager');
const { generateDidDocument } = require('@services/didDocument');


/**
 * Send the list of VC can be issued
 */
exports.listVCtypes = async (req, res, next) => {
  try {
    const gatewayUrl = routingTable.getConnectionUrl(req.params.gateway, 'issuance');
    const response = await axios.get(gatewayUrl, { headers: { 'origin': req.hostname } });
    logger.info(`The VC list was successfully retrieved`);
    res.status(200).json(response.data);
  }
  catch (error) {
    next(error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
  }
};

exports.issuance = async (req, res, next) => {
  try {
    // Checking the version to prevent file path injection attacks
    validateOidcVersion(req.body.issuance_options.oid4vci_version);

    if (req.body.action === 'startIssuance') {
      const gatewayUrl = `${routingTable.getConnectionUrl(req.params.gateway, 'issuance')}/${req.params.vcType}`;
      let response = await axios.post(gatewayUrl, { action: 'startIssuance', typeOfIssuance: req.body.type_of_issuance });
      switch (response.data.type) {
        case 'form': {
          res.status(200).json({ form: response.data.form });
          break;
        }
        case 'presentation': {
          validateOidcVersion(req.body.presentation_options.oid4vp_version);
          const { openidUri, state } = await presentationService.createNewPresentation({
            gateway: req.params.gateway,
            presentation_options: {
              gateway_service: req.params.vcType,
              ...req.body.presentation_options
            },
            issuance_options: req.body.issuance_options,
            presentation_definition_name: response.data.presentation
          });
          logger.info(`The contiueIssuance for VC ${req.params.vcType} was successful`, { openidPresentationUri: openidUri });
          res.status(200).json({ openidUri: openidUri, state: state });
          break;
        }
        case 'keycloak': {
          res.status(200).json({ type: 'keycloak', redirectUri: response.data.redirectUri });
          break;
        }
        default:
          throw new HttpError({ message: 'Invalid_request', error_description: `Type of VC not supported: ${req.params.vcType}`, status_code: 400 });
      }
      logger.info(`The startIssuance for VC ${req.params.vcType} was successful`);
    }
    else if (req.body.action === 'continueIssuance') {
      // Build the data object for issuance request in gateway
      const data = { action: 'continueIssuance', ...req.body.data };

      // Get credential and credential type to issue
      const credentialInformations = await issuanceService.getCredentialInformations(req.params.gateway, req.params.vcType, data);

      // Create New Pre Authorized Workflow
      const openidIssuanceUri = await issuanceService.createNewIssuance({ gateway: req.params.gateway, issuance_options: req.body.issuance_options, credentials_information: credentialInformations });
      res.status(200).json({ openidUri: openidIssuanceUri });
    }
    else {
      throw new HttpError({ message: 'Invalid_request', error_description: `Action not supported: ${req.body.action}`, status_code: 400 });
    }
  }
  catch (error) {
    next(error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
  }
};

exports.newIssuance = async (req, res, next) => {
  try {
    const openidIssuanceUri = await issuanceService.createNewIssuance({ gateway: req.params.gateway, issuance_options: req.body.issuance_options, credentials_information: req.body.credentials_information });
    res.status(200).json({ openidUri: openidIssuanceUri, portalUrl: `${process.env.PORTAL_URL}/issuer?openidUri=${encodeURIComponent(openidIssuanceUri)}` });
  }
  catch (error) {
    next(error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
  }
}


/**
 * Generate the DID document
 *
 * @param {Object} req - the request object
 * @param {Object} res - the response object
 * @param {Function} next - the next middleware function
 * @return {Promise<void>} - a promise that resolves with the retrieved configuration as a JSON response
 */
exports.getDidConfiguration = async (req, res, next) => {
  try {
    const didDoc = await generateDidDocument();
    res.json(didDoc);
  }
  catch (error) {
    next(new HttpError({ message: 'Error to get did.json', error_description: error.message, status_code: 500 }));
  }
}


exports.jwks = async (req, res, next) => {
  try {
    const privateJwk = JSON.parse(process.env.PRIVATE_KEY);
    const keyObject = createPublicKey({ key: privateJwk, format: 'jwk' });
    const publicKeyJwk = await exportJWK(keyObject);

    if (publicKeyJwk.kty === 'RSA') {
      publicKeyJwk.alg = privateJwk.alg || 'PS256';
    }
    if (privateJwk.kid !== undefined && privateJwk.kid !== '') publicKeyJwk.kid = privateJwk.kid;

    res.status(200).json({
      keys: [publicKeyJwk]
    });
  }
  catch (error) {
    throw (error instanceof HttpError ? error : new HttpError({ message: 'Error in jwks', error_description: error.message, status_code: 500 }));
  }
}