const axios = require('axios');
const utils = require("@utils/utils");
const { v4: uuidv4 } = require('uuid');
const logger = require('@services/logger');
const HttpError = require('@utils/httpError');
const issuanceService = require('@services/issuance');
const presentationService = require('@services/presentation');
const routingTable = require('@services/routingTableManager');
const { put, getOne, deleteOne } = require('@services/mongodb');
const { decodeSdJwtCredential } = require('@services/credential');

exports.newPresentation = async (req, res, next) => {
    try {
        const { openidUri, state } = await presentationService.createNewPresentation({ 
            gateway: req.params.gateway, 
            presentation_options: req.body.presentation_options,
            issuance_options: req.body.issuance_options,
            presentation_definition_name: req.body.presentation_definition 
        });
        res.status(200).json({ openidUri: openidUri, portalUrl: `${process.env.PORTAL_URL}/verifier?openidUri=${encodeURIComponent(openidUri)}&state=${state}`, state: state });
    }
    catch (error) {
        next(error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
    }
}


exports.getPresentationResult = async (req, res, next) => {
    try {
        const vpRequest = await getOne("PresentationRequest", req.params.state);
        if (vpRequest.status && vpRequest.status === "Pending") {
            res.status(200).json({ message: "Pending" });
        }
        else if (vpRequest.status && vpRequest.status === "Received" && vpRequest.vp) {
            if (vpRequest.gateway_service && vpRequest.gateway_service !== '') {
                // Build the data object for issuance request in gateway
                const data = { action: 'continueIssuance', vp: JSON.parse(vpRequest.vp) };

                // Get credential and credential type to issue
                const credentialInformations = await issuanceService.getCredentialInformations(req.params.gateway, vpRequest.gateway_service, data);
            
                // Create New Issuance
                const openidIssuanceUri = await issuanceService.createNewIssuance({ gateway: req.params.gateway, issuance_options: vpRequest.issuance_options, credentials_information: credentialInformations });
                res.status(200).json({ openidUri: openidIssuanceUri, portalUrl: `${process.env.PORTAL_URL}/issuer?openidUri=${openidIssuanceUri}` });
            }
            else {
                if (utils.isJson(vpRequest.vp)) {
                    // Send the ldp_vp
                    res.status(200).json({ format: 'ldp_vp', vp: JSON.parse(vpRequest.vp) });
                }
                else {
                    // Decode the sd-jwt and send it
                    const { claims, decodedSdJwt } = await decodeSdJwtCredential(vpRequest.vp);
                    res.status(200).json({ format: 'vc+sd-jwt', rawJWT: vpRequest.vp, claims, decodedSdJwt });
                }
            }
        }
        else {
            throw new HttpError({ message: 'Invalid_request', error_description: `Presentation not found for state ${req.params.state}`, status_code: 404 });
        }
    } catch (error) {
        next(error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
    }
}

exports.authorize = async (req, res, next) => {
    try {
        if (!req.body.state || req.body.state == '')
            throw new HttpError({ message: 'Invalid_request', error_description: 'Invalid state', status_code: 400 });

        if (!req.body.username || req.body.username !== '1234')
            throw new HttpError({ message: 'Access_denied', error_description: 'Login failes', status_code: 401 });

        // Get object from database
        const dataAuthorizationCode = await getOne('CredentialsRequest', req.body.state);
        if (!dataAuthorizationCode || !dataAuthorizationCode.hasOwnProperty('credentials_information') || !dataAuthorizationCode.hasOwnProperty('state') || !dataAuthorizationCode.hasOwnProperty('redirect_uri'))
            throw new HttpError({ message: 'Invalid_request', error_description: 'Invalid state', status_code: 400 });

        const state = dataAuthorizationCode.state;
        const newDataAuthorizationCode = {
            credentials_information: dataAuthorizationCode.credentials_information,
            authorization_code: uuidv4(),
            code_challenge: dataAuthorizationCode.code_challenge,
            code_challenge_methode: dataAuthorizationCode.code_challenge_method,
            state: state
        }

        // Save object in database
        await deleteOne('CredentialsRequest', req.body.state);
        await put('CredentialsRequest', { id: newDataAuthorizationCode.authorization_code, object: newDataAuthorizationCode });
        const separator = dataAuthorizationCode.redirect_uri.includes('?') ? '&' : '?';
        let redirectUrl = `${dataAuthorizationCode.redirect_uri}${separator}code=${newDataAuthorizationCode.authorization_code}`;
        if (state !== null) {
            redirectUrl += `&state=${state}`;
        }
        logger.info('Login success', { "code": newDataAuthorizationCode.authorization_code, "state": state, redirectUrl: redirectUrl });
        res.status(200).json({ redirect_url: redirectUrl });
    }
    catch (error) {
        next(error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
    }
}


/**
 * Get the presentation definitions. Used for some test in the portal
 */
exports.getPresentationDefinitions = async (req, res, next) => {
    try {
        const gatewayUrl = routingTable.getConnectionUrl(req.params.gateway, 'presentation_definitions');
        const response = await axios.get(gatewayUrl);
        res.status(200).json(response.data);
    }
    catch (error) {
        next(error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
    }
}