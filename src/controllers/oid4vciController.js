const HttpError = require('@utils/httpError');
const { validateOidcVersion } = require('@utils/utils');


exports.par = (req, res, next) => handleRequest(req, res, next, "par");
exports.jwks = (req, res, next) => handleRequest(req, res, next, "jwks");
exports.token = (req, res, next) => handleRequest(req, res, next, "token");
exports.deferred = (req, res, next) => handleRequest(req, res, next, "deferred");
exports.authorize = (req, res, next) => handleRequest(req, res, next, "authorize");
exports.credential = (req, res, next) => handleRequest(req, res, next, "credential");
exports.getOpenidConfiguration = (req, res, next) => handleRequest(req, res, next, "getOpenidConfiguration");
exports.getOauthAuthorizationServer = (req, res, next) => handleRequest(req, res, next, "getOauthAuthorizationServer");


async function handleRequest(req, res, next, action) {
  try {
    // Check OID4VCI version requested
    validateOidcVersion(req.params.oid4vci_version);

    const oid4vciModule = await import(`../services/oid4vci/${req.params.oid4vci_version}.js`);
    if (oid4vciModule[action]) {
      const result = await oid4vciModule[action](req);

      if (action === 'authorize') {
        res.redirect(302, result.url);
      }
      else {
        res.setHeader('Cache-Control', 'no-store');
        res.status(200).json(result);
      }
    } else {
      throw new HttpError({ message: 'Invalid_request', error_description: 'The endpoint doesn\'t exist', status_code: 404 });
    }
  } catch (error) {
    next(error instanceof HttpError ? error : new HttpError({ message: error.message, status_code: 500 }));
  }
}