const cors = require('cors');
require('module-alias/register');
const express = require('express');
const bodyParser = require('body-parser');
const logger = require('./services/logger');
const HttpError = require('./utils/httpError');
const swaggerUi = require('swagger-ui-express');
const { initJWKs } = require('./services/didDocument');
const swaggerDocument = require('./swagger/openapi.json');
const oid4VpController = require('./controllers/oid4VpController');
const oid4vciController = require('./controllers/oid4vciController');
const issuanceController = require('./controllers/issuanceController');
const revocationController = require('./controllers/revocationController');
const presentationController = require('./controllers/presentationControllers');


/* For local development */
if (process.env.NODE_ENV !== "production") require("dotenv").config();

/* Express Configuration */
const PORT = 3002;
const app = express();
app.use(express.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use((req, res, next) => {
  const logObject = {};
  if (req.body && Object.keys(req.body).length !== 0) logObject.body = req.body;
  if (req.query && Object.keys(req.query).length !== 0) logObject.query = req.query;
  if (req.headers.authorization) logObject.Authorization = req.headers.authorization;
  logger.info(`Received request on ${req.method} ${req.path}`, logObject);
  next();
});

/* Database creation */
const { init } = require("./services/mongodb");
(async () => {
  await init();
  await initJWKs();
})();

/* Middleware: API Key verification */
function verifyApiKey(req, res, next) {
  if (req.headers['x-api-key'] && req.headers['x-api-key'] === process.env.API_KEY_AUTHORIZED) {
    next();
  } else {
    res.status(401).json({ error: 'Unauthorised: wrong api key' });
  }
}

/* Swagger */
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

/* OIDC Configuration Route */
app.get('/:gateway/issuer/:oid4vci_version/.well-known/openid-configuration', oid4vciController.getOpenidConfiguration);
app.get('/:gateway/issuer/:oid4vci_version/.well-known/openid-credential-issuer', oid4vciController.getOpenidConfiguration);
app.get('/:gateway/issuer/:oid4vci_version/.well-known/oauth-authorization-server', oid4vciController.getOauthAuthorizationServer);
app.get('/:gateway/issuer/.well-known/did.json', issuanceController.getDidConfiguration);
app.get('/.well-known/did.json', issuanceController.getDidConfiguration);
app.get('/.well-known/jwks', issuanceController.jwks);

/* OID4VCI Route */
app.get('/:gateway/issuer/:oid4vci_version/jwks', oid4vciController.jwks);
app.post('/:gateway/issuer/:oid4vci_version/token', oid4vciController.token);
app.post('/:gateway/issuer/:oid4vci_version/authorize/par', oid4vciController.par);
app.get('/:gateway/issuer/:oid4vci_version/authorize', oid4vciController.authorize);
app.post('/:gateway/issuer/:oid4vci_version/credential', oid4vciController.credential);
app.post('/:gateway/issuer/:oid4vci_version/deferred', oid4vciController.deferred);

/* OID4VP Route */
app.get('/:gateway/verifier/:oid4vp_version/request_uri/:state', oid4VpController.getRequestUriResult);
app.post('/:gateway/verifier/:oid4vp_version/:state', oid4VpController.callbackPresentation);

/* Issuance Portal Route */
app.get('/:gateway/issuance', issuanceController.listVCtypes);
app.post('/:gateway/issuance/:vcType', issuanceController.issuance);
app.post('/:gateway/issuance', issuanceController.newIssuance);

/* Presentation Controller */
app.post('/:gateway/presentation', presentationController.newPresentation);
app.get('/:gateway/presentation/:state', presentationController.getPresentationResult);
app.post('/:gateway/presentation/authorize', presentationController.authorize); // For Authorized workflow
app.get('/:gateway/presentation-definitions', presentationController.getPresentationDefinitions);


/* Revocation Portal Route */
app.get('/revocations', revocationController.getCredentials);
app.get('/revocations/:idVC', revocationController.getStatus);
app.post('/revocations/:idVC', revocationController.updateStatus);

/* Error */
app.use((error, req, res, next) => {
  logger.error(error.message, { error_description: error.error_description, stackTrace: error.stack ? error.stack : 'No stack trace' });
  if (error instanceof HttpError && error.status_code !== 500) {
    res.status(error.status_code).json({ error: error.message, error_description: error.error_description || 'No error description' });
  } else {
    res.status(500).json({ error: 'Internal server error', error_description: 'Internal server error'});
  }
});

/* Start the server */
app.listen(PORT, () => {
  logger.info(`Server listening on port ${PORT}`);
});