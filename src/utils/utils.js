const jwt = require('jsonwebtoken');
const HttpError = require('./httpError');


/**
 * This function takes a VP in JWT format as a parameter and decodes it.
 * @param {object} vp The verifiable presentation in JWT format
 * @returns The decoded verifiable presentation
 */
exports.decodeVP = (vp) => {
  const decodedVP = { ...vp };
  for (let i = 0; i < decodedVP.verifiableCredential.length; i++) {    
    const decodedVC = jwt.decode(decodedVP.verifiableCredential[i], { complete: true });
    decodedVP.verifiableCredential[i] = decodedVC.payload.vc;
  }

  return decodedVP;
}


/**
 * Replaces placeholders in the form of {{KEY}} in a JSON document with the corresponding values provided.
 * @param {object} json The JSON object in which to perform the replacements. Must be a valid object and not a JSON string.
 * @param {object} values An object containing key/value pairs where each key corresponds to a placeholder in JSON and each value is the replacement value.
 * @returns {object} A new JSON object with the placeholders replaced by the corresponding values. If `values` does not contain keys corresponding to the placeholders in `json`, the returned object remains unchanged for those placeholders.
 */
exports.replaceValuesInJSON = (json, values) => {
  let result = JSON.stringify(json);
  for (const key of Object.keys(values)) {
    const regex = new RegExp(`{{${key}}}`, 'g');
    result = result.replace(regex, values[key]);
  }
  return JSON.parse(result);
}

/**
 * Validates the input OIDC version string to prevent file path injection attacks
 * @param {string} oidcVersion - The OIDC version to validate.
 * @returns {boolean} - True if the OIDC version is valid.
 * @throws {HttpError} - If the OIDC version contains potentially malicious characters.
 */
exports.validateOidcVersion = (oidcVersion) => {
  if (!RegExp(/^[a-zA-Z0-9-_]+$/).exec(oidcVersion)) {
    logger.warn("Potential threat to user input regarding OIDC version.", { "Requested version OIDC": oidcVersion });
    throw new HttpError({ message: 'The version doesn\'t exist', status_code: 404 });
  }
  return true;
}


/**
 * Verify if the string is valid JSON
 * @param {*} str The string to check
 * @returns {boolean} - True if the string is valid JSON
 */
exports.isJson = (str) => {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}