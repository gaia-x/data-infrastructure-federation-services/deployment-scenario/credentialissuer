class HttpError extends Error {
    constructor({ message, error_description, status_code }) {
        super(message);
        this.error_description = error_description;
        this.status_code = status_code;
    }
}

module.exports = HttpError;